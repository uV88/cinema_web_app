<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:choose>
    <c:when test="${requestScope.simpleMovieDTO.id gt 0}">
        <c:set value="Edit movie" var="titleVar"/>
    </c:when>
    <c:otherwise>
        <c:set value="Add movie" var="titleVar"/>
    </c:otherwise>
</c:choose>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="${titleVar}"/>
    <c:param name="noAuth" value="noAuth"/>
    <c:param name="noNav" value="noNav"/>
</c:import>

<form accept-charset="UTF-8" action="<c:url value="/implmovie"/>" method="post">
    <input type="hidden" name="movieId" value="${requestScope.simpleMovieDTO.id}">
    <ul>
        <li>
            <label for="title">Title:</label><input type="text" id="title" required name="title" value="${requestScope.simpleMovieDTO.movieTitle}">
        </li>
        <li>
            <label for="orTitle">Original title:</label><input type="text" id="orTitle" name="orTitle" value="${requestScope.simpleMovieDTO.originalMovieTitle}">
        </li>
        <li>
            <label for="posterUrl">Poster url:</label><input type="text" id="posterUrl" name="posterUrl" value="${requestScope.simpleMovieDTO.posterUrl}">
        </li>
        <li>
            <label for="description">Description:</label><textarea id="description" required name="description">${requestScope.simpleMovieDTO.movieDescription}</textarea>
        </li>
        <li>
            <label for="director">Director:</label><input type="text" id="director" name="director" value="${requestScope.simpleMovieDTO.director}">
        </li>
        <li>
            <label for="screenwriter">Screenwriter:</label><input type="text" id="screenwriter" name="screenwriter" value="${requestScope.simpleMovieDTO.screenwriter}">
        </li>
        <li>
            <label for="actors">Actors:</label><textarea id="actors" name="actors">${requestScope.simpleMovieDTO.actors}</textarea>
        </li>
        <li>
            <input type="submit" value="OK"> <a href="${sessionScope.url}">CANCEL</a>
        </li>
    </ul>
</form>
</body>
</htl>
