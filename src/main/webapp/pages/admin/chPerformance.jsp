<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:choose>
    <c:when test="${requestScope.changedSimplePerformanceDTO.id gt 0}">
        <c:set value="Edit performance" var="titleVar"/>
    </c:when>
    <c:otherwise>
        <c:set value="Add performance" var="titleVar"/>
    </c:otherwise>
</c:choose>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="${titleVar}"/>
    <c:param name="noAuth" value="noAuth"/>
    <c:param name="noNav" value="noNav"/>
</c:import>

<form accept-charset="UTF-8" action="<c:url value="/implperfor"/>" method="post">
    <input type="hidden" name="identification" value="${requestScope.identification}">
    <ul>
        <li>
            Movie: ${requestScope.changedSimplePerformanceDTO.simpleMovieDTO.movieTitle}
        </li>
        <li>
            Hall: ${requestScope.changedSimplePerformanceDTO.simpleHallDTO.hallName}
        </li>
        <li>
            <label for="date">Date (YYYY-MM-DD, example: 2013-10-30) </label>
            <input type="text" id="date" required name="date" value="${requestScope.changedSimplePerformanceDTO.date}">
        </li>
        <li>
            <label for="time">Time (HH:MM, example: 20:50) </label>
            <input type="text" id="time" required name="time" value="${requestScope.changedSimplePerformanceDTO.time}">
        </li>
        <li>
            <label for="forSale">Sell tickets </label>
            <input type="checkbox" id="forSale" name="forSale" value="forSale"
            <c:if test="${requestScope.changedSimplePerformanceDTO.forSale}">
                    checked
            </c:if>>
        </li>
        <c:if test="${requestScope.changedSimplePerformanceDTO.id < 0}">
            <li>Price of seat of each row: </li>
            <li>
                <c:forEach items="${requestScope.changedSimplePerformanceDTO.pricesForRows}" varStatus="rowStatus">
                <label for="price${rowStatus.index}">${rowStatus.count}</label>
                    <input type="number" id="price${rowStatus.index}" required name="price${rowStatus.index}" value="50">
                </c:forEach>
            </li>
        </c:if>
        <li>
            <input type="submit" value="OK"> <a href="<c:url value="/implperfor">
                        <c:param name="identification" value="${requestScope.identification}"/>
            </c:url>">CANCEL</a>
        </li>
    </ul>
</form>
</body>
</html>
