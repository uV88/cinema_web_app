<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp" charEncoding="UTF-8">
    <c:param name="titleString" value="All halls"/>
</c:import>

<h1>
    All halls
</h1>
<ul>
        <li>
            <form action="<c:url value="/purposehall"/>" method="post">
                <input type="submit" value="ADD"/>
            </form>
        </li>
    <c:forEach var="hall" items="${requestScope.simpleHallDTOList}">
        <li>
            <p>
                <a href="<c:url value="/hall">
                <c:param name="id" value="${hall.id}"/>
            </c:url>">
                        ${hall.hallName}
                </a>
            </p>
            <p>
                number of seats: ${hall.seatsNumber}
            </p>
            <form action="<c:url value="/purposeperfor"/>" method="post">
                <input type="hidden" name="hallId" value="${hall.id}"/>
                <input type="submit" value="ADD PERFORMANCE"/>
            </form>
        </li>
    </c:forEach>
</ul>
</body>
</html>
