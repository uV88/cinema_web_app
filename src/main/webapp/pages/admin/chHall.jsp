<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:choose>
    <c:when test="${requestScope.changedHallDTO.id gt 0}">
        <c:set value="Edit hall" var="titleVar"/>
    </c:when>
    <c:otherwise>
        <c:set value="Add hall" var="titleVar"/>
    </c:otherwise>
</c:choose>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="${titleVar}"/>
    <c:param name="noAuth" value="noAuth"/>
    <c:param name="noNav" value="noNav"/>
</c:import>

<form accept-charset="UTF-8" action="<c:url value="/implhall"/>" method="post">
    <input type="hidden" name="identification" value="${requestScope.identification}">
    <ul>
        <li>
            <label for="name">Hall name:</label>
            <input type="text" id="name" required name="name" value="${requestScope.changedHallDTO.hallName}">
        </li>
        <c:if test="${requestScope.changedHallDTO.id < 0}">
        <li>
            <label for="seats">Number of seats in each row (separate by whitespace): </label>
            <input type="text" id="seats" required name="seats">
        </li>
        </c:if>
        <li>
            <input type="submit" value="OK"> <a href="<c:url value="/implhall">
            <c:param name="identification" value="${requestScope.identification}"/>
            </c:url>">CANCEL</a>
        </li>
    </ul>
</form>
</body>
</htl>
