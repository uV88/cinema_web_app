<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp" charEncoding="UTF-8">
    <c:param name="titleString" value="All visitors"/>
</c:import>

<h1>
    All visitors
</h1>
<ul>
    <li>
        <a href="<c:url value="/adminregistration"/>">ADD</a>
    </li>
    <c:forEach var="visitor" items="${requestScope.simpleVisitorDTOList}">
        <li>
            <p>
                <a href="<c:url value="/visitor">
                <c:param name="id" value="${visitor.id}"/>
            </c:url>">
                        ${visitor.login}
                </a>
            </p>
            <p>
                ${visitor.firstName} ${visitor.lastName}
            </p>
            <p>
                (${visitor.role})
            </p>
        </li>
    </c:forEach>
</ul>
</body>
</html>
