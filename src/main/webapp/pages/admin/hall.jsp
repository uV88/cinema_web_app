<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp" charEncoding="UTF-8">
    <c:param name="titleString" value="${requestScope.simpleHallDTO.hallName}"/>
</c:import>
<h1>
    ${requestScope.simpleHallDTO.hallName}
</h1>
<p>
    Total number of seats: ${requestScope.simpleHallDTO.seatsNumber}
</p>
<table>
    <tr>
        <th>
            Row number
        </th>
        <th>
            Number of seats
        </th>
    </tr>
    <c:forEach items="${requestScope.simpleHallDTO.seatsScheme}" var="seatsInRow" varStatus="rowStatus">
        <tr>
            <td>
                ${rowStatus.count}
            </td>
            <td>
                ${seatsInRow}
            </td>
        </tr>
    </c:forEach>
</table>
    <form action="<c:url value="/delhall"/>" method="post">
        <input type="hidden" name="hallId" value="${requestScope.simpleHallDTO.id}"/>
        <input type="submit" value="DELETE"/>
    </form>
    <form action="<c:url value="/purposehall"/>" method="post">
        <input type="hidden" name="hallId" value="${requestScope.simpleHallDTO.id}"/>
        <input type="submit" value="EDIT"/>
    </form>
<form action="<c:url value="/purposeperfor"/>" method="post">
    <input type="hidden" name="hallId" value="${requestScope.simpleHallDTO.id}"/>
    <input type="submit" value="ADD PERFORMANCE"/>
</form>
</body>
</html>
