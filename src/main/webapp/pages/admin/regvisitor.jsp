<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp" charEncoding="UTF-8">
    <c:param name="titleString" value="Registration"/>
    <c:param name="noAuth" value="noAuth"/>
    <c:param name="noNav" value="noNav"/>
</c:import>

<form accept-charset="UTF-8" action="<c:url value="/adminregistration"/>" method="post">
    <ul>
        <li>
            <label for="login">Login:</label><input type="text" id="login" required name="login">
        </li>
        <li>
            <label for="password">Password:</label><input type="password" id="password" required name="password">
        </li>
        <li>
            <label for="confPassword">Confirm password:</label><input type="password" id="confPassword" required name="confPassword">
        </li>
        <li>
            <label for="emailAddress">Email address:</label><input type="email" id="emailAddress" required name="emailAddress">
        </li>
        <li>
            <label for="firstName">First name:</label><input type="text" id="firstName" required name="firstName">
        </li>
        <li>
            <label for="lastName">Last name:</label><input type="text" id="lastName" name="lastName">
        </li>
        <li>
            <label for="user">User</label>
            <input type="radio" id="user" name="role" value="USER" checked>
            <label for="admin">Admin</label>
            <input type="radio" id="admin" name="role" value="ADMIN">
        </li>
        <li>
            <input type="submit" value="OK"> <a href="${sessionScope.url}">CANCEL</a>
        </li>
    </ul>
</form>
</body>
</html>