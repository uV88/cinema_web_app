<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="icon" type="image/x-icon" href="<c:url value="/pic/cinemaCat.ico" />">
    <%request.setCharacterEncoding("utf8");%>
    <title>Cinema - ${param.titleString}</title>
</head>
<body>
<c:if test="${empty param.noAuth}">
<header>
    <c:choose>
        <c:when test="${not empty sessionScope.visitor}">
            <a href="<c:url value="/me"/>">
                    ${sessionScope.visitor.login} (${sessionScope.visitor.role})
            </a>
            <a href="<c:url value="/out"/>">Logout</a>
        </c:when>
        <c:otherwise>
            <a href="<c:url value="/auth"/>">Login</a>
            <a href="<c:url value="/addme"/>">Registration</a>
        </c:otherwise>
    </c:choose>
</header>
</c:if>
<c:if test="${empty param.noNav}">
<nav>
    <ul>
        <li>
            <a href="<c:url value="/movie" />">All movies</a>
        </li>
        <li>
            <a href="<c:url value="/performance"/>">Schedule</a>
        </li>
        <c:if test="${not empty sessionScope.visitor && sessionScope.visitor.role eq \"ADMIN\"}">
        <li>
            <a href="<c:url value="/hall"/>">All halls</a>
        </li>
            <li>
                <a href="<c:url value="/visitor"/>">All visitors</a>
            </li>
        </c:if>
    </ul>
</nav>
</c:if>
${sessionScope.url}
