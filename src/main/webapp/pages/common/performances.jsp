<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="Schedule"/>
</c:import>
<h1>
    Performance's list
</h1>
<ul>
    <c:if test="${not empty sessionScope.visitor && sessionScope.visitor.role eq 'ADMIN'}">
        <li>
            <form action="<c:url value="/purposeperfor"/>" method="post">
                <input type="submit" value="ADD"/>
            </form>
        </li>
    </c:if>
    <c:forEach items="${requestScope.simplePerformanceDTOList}" var="performance">
        <li>
            <p>
                <a href="<c:url value="/performance">
                <c:param name="id" value="${performance.id}"/>
            </c:url>">
                        Date: ${performance.date},
                        time: ${performance.time}.
                </a>
            </p>
            <p>
                Movie: ${performance.simpleMovieDTO.movieTitle};
            </p>
            <p>
                    Hall: ${performance.simpleHallDTO.hallName}
            </p>
            <c:if test="${performance.forSale}">
                <p>Tickets are on sale.</p>
            </c:if>
        </li>
    </c:forEach>
</ul>
</body>
</html>
