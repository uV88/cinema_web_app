<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp" charEncoding="UTF-8">
    <c:param name="titleString" value="${requestScope.movieDTO.movieTitle}"/>
</c:import>
<h1>
    ${requestScope.movieDTO.movieTitle}
</h1>
<h2>
    (${requestScope.movieDTO.originalMovieTitle})
</h2>
<img src="${requestScope.movieDTO.posterUrl}"/>
<p>
    ${requestScope.movieDTO.movieDescription}
</p>
<p>
    Director: ${requestScope.movieDTO.director}.
</p>
<p>
    Screenwriter: ${requestScope.movieDTO.screenwriter}.
</p>
<p>
    Actors: ${requestScope.movieDTO.actors}.
</p>
<c:if test="${not empty sessionScope.visitor && sessionScope.visitor.role eq 'ADMIN'}">
    <form accept-charset="UTF-8" action="<c:url value="/addposter"/>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="movieId" value="${requestScope.movieDTO.id}">
        Select image for movie poster: <input type="file" name="file" size="60" />
        <input type="submit" value="DOWNLOAD POSTER"/>
    </form>
    <form action="<c:url value="/purposemovie"/>" method="post">
        <input type="hidden" name="movieId" value="${requestScope.movieDTO.id}"/>
        <input type="submit" value="EDIT"/>
    </form>
    <form action="<c:url value="/delmovie"/>" method="post">
        <input type="hidden" name="movieId" value="${requestScope.movieDTO.id}"/>
        <input type="submit" value="DELETE"/>
    </form>
    <form action="<c:url value="/purposeperfor"/>" method="post">
        <input type="hidden" name="movieId" value="${requestScope.movieDTO.id}"/>
        <input type="submit" value="ADD PERFORMANCE"/>
    </form>
</c:if>
<ul>
    <c:forEach items="${requestScope.movieDTO.simplePerformancesDTO}" var="performance">
    <li>
        <p>
            <a href="<c:url value="/performance">
                <c:param name="id" value="${performance.id}"/>
            </c:url>">
                    ${performance.date}
                    ${performance.time}
            </a>
        </p>
        <p>
            Hall: ${performance.simpleHallDTO.hallName}.
        </p>
    </li>
    </c:forEach>
</ul>
</body>
</html>
