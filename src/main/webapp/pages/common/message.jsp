<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="Message"/>
    <c:param name="noAuth" value="noAuth"/>
    <c:param name="noNav" value="noNav"/>
</c:import>

<p>${requestScope.message}</p>
<p>
    <a href="${requestScope.returnUrl}">
        OK
    </a>
</p>
</body>
</html>
