<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="Buying a ticket"/>
</c:import>

<c:choose>
    <c:when test="${requestScope.ticketDTO.id ne (-1)}">
        <h1>You bought a ticket!</h1>
        <p>Movie: ${requestScope.ticketDTO.simplePerformanceDTO.simpleMovieDTO.movieTitle}</p>
        <p>Date: ${requestScope.ticketDTO.simplePerformanceDTO.date}</p>
        <p>Time: ${requestScope.ticketDTO.simplePerformanceDTO.time}</p>
        <p>Hall: ${requestScope.ticketDTO.simplePerformanceDTO.simpleHallDTO.hallName}</p>
        <p>Row: ${requestScope.ticketDTO.rowNumber + 1}</p>
        <p>Seat: ${requestScope.ticketDTO.seatNumber + 1}</p>
        <p>Price: ${requestScope.ticketDTO.price}</p>
        <p>Owner: ${requestScope.ticketDTO.simpleVisitorDTO.firstName} ${requestScope.ticketDTO.simpleVisitorDTO.lastName}</p>
        <p>
            <a href="<c:url value="/performance">
            <c:param name="id" value="${requestScope.ticketDTO.simplePerformanceDTO.id}"/>
        </c:url>">
            Buy another one
            </a>
        </p>
    </c:when>
    <c:otherwise>
        <h1>Something went wrong</h1>
        <a href="<c:url value="/performance">
            <c:param name="id" value="${requestScope.ticketDTO.simplePerformanceDTO.id}"/>
        </c:url>">
            Try again
        </a>
    </c:otherwise>
</c:choose>
</body>
</html>
