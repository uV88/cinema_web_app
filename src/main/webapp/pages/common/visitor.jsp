<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp" charEncoding="UTF-8">
    <c:param name="titleString" value="${requestScope.visitorDTO.login}"/>
</c:import>

<h1>
    ${requestScope.visitorDTO.login}
</h1>
<p>
    First name:${requestScope.visitorDTO.firstName}
</p>
<p>
    Last name: ${requestScope.visitorDTO.lastName}
</p>
<p>
    Email: ${requestScope.visitorDTO.emailAddress}
</p>
<p>
    Role: ${requestScope.visitorDTO.role}
</p>
<c:if test="${sessionScope.visitor.id eq requestScope.visitorDTO.id}">
    <a href="<c:url value="/editme"/>">EDIT</a>
</c:if>
<table>
    <caption>Tickets</caption>
    <tr>
        <th>
            Movie
        </th>
        <th>
            Hall
        </th>
        <th>
            Date
        </th>
        <th>
            Time
        </th>
        <th>
            Row
        </th>
        <th>
            Seat
        </th>
        <th>
            Price
        </th>
        <c:if test="${sessionScope.visitor.role eq 'ADMIN'}">
            <th>
            repayment
            </th>
        </c:if>
    </tr>
    <c:forEach items="${requestScope.visitorDTO.ticketsDTO}" var="soldTicket">
        <tr>
            <td>
                ${soldTicket.simplePerformanceDTO.simpleMovieDTO.movieTitle}
            </td>
            <td>
                    ${soldTicket.simplePerformanceDTO.simpleHallDTO.hallName}
            </td>
            <td>
                    ${soldTicket.simplePerformanceDTO.date}
            </td>
            <td>
                    ${soldTicket.simplePerformanceDTO.time}
            </td>
            <td>
                    ${soldTicket.rowNumber + 1}
            </td>
            <td>
                    ${soldTicket.seatNumber + 1}
            </td>
            <td>
                    ${soldTicket.price}
            </td>
            <c:if test="${sessionScope.visitor.role eq 'ADMIN'}">
            <td>
                <input title="mark for repayment" form="repayForm" type="checkbox" name="ticketId" value="${soldTicket.id}">
            </td>
            </c:if>
        </tr>
    </c:forEach>
</table>
<c:if test="${sessionScope.visitor.role eq 'ADMIN'}">
<form id="repayForm" accept-charset="UTF-8" action="<c:url value="/repayment"/>" method="post">
    <input type="hidden" name="visitorId" value="${requestScope.visitorDTO.id}"/>
    <input type="submit" value="REPAY">
</form>
<c:if test="${requestScope.visitorDTO.role ne 'ADMIN'}">
<c:choose>
    <c:when test="${requestScope.visitorDTO.blocked}">
        <form action="<c:url value="/unblockvisitor"/>" method="post">
            <input type="hidden" name="visitorId" value="${requestScope.visitorDTO.id}">
            <input type="submit" name="action" value="UNBLOCK"/>
        </form>
    </c:when>
    <c:otherwise>
        <form action="<c:url value="/blockvisitor"/>" method="post">
            <input type="hidden" name="visitorId" value="${requestScope.visitorDTO.id}">
            <input type="submit" name="action" value="BLOCK"/>
        </form>
    </c:otherwise>
</c:choose>
</c:if>
</c:if>
</body>
</html>