<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="Authorization"/>
    <c:param name="noAuth" value="noAuth"/>
    <c:param name="noNav" value="noNav"/>
</c:import>

<p>${requestScope.message}</p>
<form accept-charset="UTF-8" action="<c:url value="/auth"/>" method="post">
    <ul>
        <li>
            <label for="login">login: </label>
            <input type="text" name="login" id="login"/>
        </li>
        <li>
            <label for="password">password: </label>
            <input type="password" name="password" id="password"/>
        </li>
        <li>
            <input type="submit" value="OK"/> <a href="${sessionScope.url}">CANCEL</a>
        </li>
    </ul>
</form>
</body>
</html>
