<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="All movies"/>
</c:import>
<h1>
    All movies
</h1>
<ul>
    <c:if test="${not empty sessionScope.visitor && sessionScope.visitor.role eq 'ADMIN'}">
        <li>
            <form action="<c:url value="/purposemovie"/>" method="post">
                <input type="submit" value="ADD"/>
            </form>
        </li>
    </c:if>
    <c:forEach var="movie" items="${requestScope.simpleMovieDTOList}">
        <li>
            <p>
                <a href="<c:url value="/movie">
                <c:param name="id" value="${movie.id}"/>
            </c:url>">
                        ${movie.movieTitle}
                </a>
            </p>
            <p>
                    (${movie.originalMovieTitle})
            </p>
            <img src="${movie.posterUrl}"/>
            <p>
                    Director: ${movie.director}.
            </p>
            <p>
                    Screenwriter: ${movie.screenwriter}.
            </p>
            <p>
                    Actors: ${movie.actors}.
            </p>
            <c:if test="${not empty sessionScope.visitor && sessionScope.visitor.role eq 'ADMIN'}">
                <form action="<c:url value="/purposeperfor"/>" method="post">
                    <input type="hidden" name="movieId" value="${movie.id}"/>
                    <input type="submit" value="ADD PERFORMANCE"/>
                </form>
            </c:if>
        </li>
    </c:forEach>
</ul>
</body>
</html>
