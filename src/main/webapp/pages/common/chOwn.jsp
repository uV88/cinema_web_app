<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:choose>
    <c:when test="${requestScope.simpleVisitorDTO.id gt 0}">
        <c:set value="Edit visitor ${requestScope.simpleVisitorDTO.login}" var="titleVar"/>
    </c:when>
    <c:otherwise>
        <c:set value="Add visitor" var="titleVar"/>
    </c:otherwise>
</c:choose>

<c:import url="/pages/pageelements/head.jsp" charEncoding="UTF-8">
    <c:param name="titleString" value="${titleVar}"/>
    <c:param name="noAuth" value="noAuth"/>
    <c:param name="noNav" value="noNav"/>
</c:import>

<form accept-charset="UTF-8" action="<c:url value="/implme"/>" method="post">
    <input type="hidden" name="id" value="${requestScope.simpleVisitorDTO.id}">
    <ul>
        <c:if test="${requestScope.simpleVisitorDTO.id lt 0}">
            <li>
                <label for="login">Login:</label><input type="text" id="login" required name="login" >
            </li>
        </c:if>
        <c:if test="${requestScope.simpleVisitorDTO.id gt 0}">
            <li>
                Login: ${requestScope.simpleVisitorDTO.login}
            </li>
            <li>
                <label for="oldPassword">Old password:</label><input type="password" id="oldPassword" required name="oldPassword">
            </li>
        </c:if>
        <li>
            <label for="password">Password:</label><input type="password" id="password" required name="password">
        </li>
        <li>
            <label for="confPassword">Confirm password:</label><input type="password" id="confPassword" required name="confPassword">
        </li>
        <li>
            <label for="emailAddress">Email address:</label><input type="email" id="emailAddress" required name="emailAddress" value="${requestScope.simpleVisitorDTO.emailAddress}">
        </li>
        <li>
            <label for="firstName">First name:</label><input type="text" id="firstName" required name="firstName" value="${requestScope.simpleVisitorDTO.firstName}">
        </li>
        <li>
            <label for="lastName">Last name:</label><input type="text" id="lastName" name="lastName" value="${requestScope.simpleVisitorDTO.lastName}">
        </li>
        <li>
            <input type="submit" value="OK"> <a href="${sessionScope.url}">CANCEL</a>
        </li>
    </ul>
</form>
</body>
</html>