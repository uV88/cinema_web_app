<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<c:import url="/pages/pageelements/head.jsp">
    <c:param name="titleString" value="${requestScope.performanceDTO.simpleMovieDTO.movieTitle} -
    ${requestScope.performanceDTO.date} ${requestScope.performanceDTO.time}"/>
</c:import>

<h1>Buying ticket</h1>
<c:if test="${not empty sessionScope.visitor && sessionScope.visitor.role eq 'ADMIN'}">
    <form action="<c:url value="/delperfor"/>" method="post">
        <input type="hidden" name="performanceId" value="${requestScope.performanceDTO.id}"/>
        <input type="submit" value="DELETE"/>
    </form>
    <form action="<c:url value="/purposeperfor"/>" method="post">
        <input type="hidden" name="performanceId" value="${requestScope.performanceDTO.id}"/>
        <input type="submit" value="EDIT"/>
    </form>
</c:if>
<p>
    Title: ${requestScope.performanceDTO.simpleMovieDTO.movieTitle}
</p>
<p>
    Original title: ${requestScope.performanceDTO.simpleMovieDTO.originalMovieTitle}
</p>
<p>
    Director: ${requestScope.performanceDTO.simpleMovieDTO.director}
</p>
<p>
    Screenwriter: ${requestScope.performanceDTO.simpleMovieDTO.screenwriter}
</p>
<p>
    Actors: ${requestScope.performanceDTO.simpleMovieDTO.actors}
</p>
<p>
    Date: ${requestScope.performanceDTO.date}
</p>
<p>
    Time: ${requestScope.performanceDTO.time}
</p>
<p>
    Hall name: ${requestScope.performanceDTO.simpleHallDTO.hallName}
</p>
<c:choose>
    <c:when test="${requestScope.performanceDTO.forSale}">
<h2>Buy ticket (prices):</h2>
<table>
    <c:forEach items="${requestScope.disposedPrices}" var="pricesInRow" varStatus="rowStatus">
        <tr>
            <c:forEach items="${pricesInRow}" var="priceOfSeat" varStatus="seatStatus">
                <td>
                    <c:if test="${priceOfSeat ne (-1)}">
                    <form action="<c:url value="/buy"/>" method="post">
                        <input type="hidden" name="performanceId" value="${requestScope.performanceDTO.id}"/>
                        <input type="hidden" name="row" value="${rowStatus.index}"/>
                        <input type="hidden" name="seat" value="${seatStatus.index}"/>
                        <input type="submit" name="price" value="${priceOfSeat}">
                    </form>
                    </c:if>
                </td>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
    </c:when>
    <c:otherwise>
        <p>Tickets are not on sale.</p>
    </c:otherwise>
</c:choose>
<c:if test="${not empty sessionScope.visitor && sessionScope.visitor.role eq 'ADMIN'}">
    <table>
        <tr>
            <th>
                Row
            </th>
            <th>
                Seat
            </th>
            <th>
                Customer
            </th>
            <th>
                Price
            </th>
            <th>
                repayment
            </th>
        </tr>
        <c:forEach items="${requestScope.performanceDTO.ticketsDTO}" var="soldTicket">
            <tr>
                <td>
                        ${soldTicket.rowNumber + 1}
                </td>
                <td>
                        ${soldTicket.seatNumber + 1}
                </td>
                <td>
                        ${soldTicket.simpleVisitorDTO.lastName} ${soldTicket.simpleVisitorDTO.firstName}
                    ( ${soldTicket.simpleVisitorDTO.login}) - ${soldTicket.simpleVisitorDTO.role}
                </td>
                <td>
                        ${soldTicket.price}
                </td>
                <td>
                    <input title="mark for repayment" form="repayForm" type="checkbox" name="ticketId" value="${soldTicket.id}">
                </td>
            </tr>
        </c:forEach>
    </table>
    <form id="repayForm" accept-charset="UTF-8" action="<c:url value="/repayment"/>" method="post">
        <input type="hidden" name="performanceId" value="${requestScope.performanceDTO.id}"/>
        <input type="submit" value="REPAY">
    </form>
</c:if>
</body>
</html>
