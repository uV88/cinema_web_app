package ua.org.oa.shadow.of.play.cinema.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class represents cinema performance.
 */
public class Performance extends Entity {

    private LocalDate date;
    private LocalTime time;
    private Hall hall;
    private Movie movie;
    private List<Integer> pricesForRows;
    private boolean forSale;
    private List<Ticket> tickets;

    /**
     * Creates new Performance object
     */
    public Performance() {
    }

    /**
     * Creates new Performance object
     * @param date - date of the performance
     * @param time - time of the performance
     * @param hall - performance's hall
     * @param movie - performance's movie
     */
    public Performance(LocalDate date, LocalTime time, Hall hall, Movie movie) {
        this.date = date;
        this.time = time;
        this.hall = hall;
        this.movie = movie;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<Integer> getPricesForRows() {
        return pricesForRows;
    }

    public void setPricesForRows(List<Integer> pricesForRows) {
        this.pricesForRows = pricesForRows;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString() {
        return "Performance{" +
                "id=" + getId() +
                ", date=" + date +
                ", time=" + time +
                ", hall=" + (hall == null ? "null" : hall.getId()) +
                ", movie=" + (movie == null ? "null" : movie.getId()) +
                '}';
    }
}
