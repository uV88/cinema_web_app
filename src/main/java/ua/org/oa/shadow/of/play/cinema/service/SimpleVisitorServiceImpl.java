package ua.org.oa.shadow.of.play.cinema.service;

import ua.org.oa.shadow.of.play.cinema.dao.api.VisitorDao;
import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;
import ua.org.oa.shadow.of.play.cinema.model.Visitor;
import ua.org.oa.shadow.of.play.cinema.service.api.VisitorService;

/**
 * Created by volodymyr on 01.04.17.
 *
 * Class implements VisitorService interface for keeping objects of class SimpleVisitorDTO.
 */
public class SimpleVisitorServiceImpl extends ServiceImpl<SimpleVisitorDTO, Visitor>
        implements VisitorService<Integer, SimpleVisitorDTO> {

    private VisitorDao<Integer, Visitor> visitorDao;

    protected SimpleVisitorServiceImpl(VisitorDao<Integer, Visitor> visitorDao) {
        super(SimpleVisitorDTO.class, Visitor.class,
                visitorDao);
        this.visitorDao = visitorDao;
    }

    @Override
    public SimpleVisitorDTO getByLogin(String login) {
        Visitor visitor = visitorDao.getByLogin(login);
        return (visitor != null) ? ServiceImpl.BEAN_MAPPER.singleMapper(visitor, SimpleVisitorDTO.class) : null;
    }

}
