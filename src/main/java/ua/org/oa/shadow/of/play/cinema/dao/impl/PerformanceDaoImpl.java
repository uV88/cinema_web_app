package ua.org.oa.shadow.of.play.cinema.dao.impl;

import ua.org.oa.shadow.of.play.cinema.model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class implements Dao interface for keeping objects of class Performance in data storage.
 */
public final class PerformanceDaoImpl extends CrudDAO<Performance> {

    private static final String SELECT_FROM ="SELECT * FROM Performance NATURAL JOIN (Movie, Hall)";
    private static final String ORDER_BY ="ORDER BY date, time";
    private static final Pattern PRICES_OF_THE_ROWS = Pattern.compile("(?<price>\\d+)\\/*");
    private static final PerformanceDaoImpl crudDAO = new PerformanceDaoImpl();

    private final String SELECT_FROM_FOR_MOVIE =
            "SELECT * FROM Performance NATURAL JOIN Hall WHERE movie_id=? ORDER BY date, time";
    private final String SELECT_FROM_FOR_HALL =
            "SELECT * FROM Performance NATURAL JOIN Movie WHERE hall_id=? ORDER BY date, time";
    private final String INSERT = "INSERT INTO Performance (date, time, movie_id, hall_id, pricesForRows, forSale) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "UPDATE Performance SET date=?, time=?, movie_id=?, hall_id=?, pricesForRows=?, forSale=? " +
            "WHERE performance_id=?";
    /**
     * Creates new MovieDaoImpl object
     */
    private PerformanceDaoImpl() {
        super(SELECT_FROM, ORDER_BY, Performance.class);
    }

    /**
     * Returns sole object of this class
     * @return PerformanceDaoImpl object
     */
    public static PerformanceDaoImpl getInstance() {
        return crudDAO;
    }

    /**
     * Returns all Performance objects, refer to specified Movie object
     * @param movie - specified object
     * @return list of performances refer to specified movie
     */
    List<Performance> selectFor(Movie movie) {
        return selectFor(movie, null);
    }

    /**
     * Returns all Performance objects, refer to specified Hall object
     * @param hall - specified object
     * @return list of performances refer to specified hall
     */
    List<Performance> selectFor(Hall hall) {
        return selectFor(null, hall);
    }

    private List<Performance> selectFor(Movie movie, Hall hall) {
        List<Performance> list = new ArrayList<>();
        String sql;
        Integer id;
        if(movie != null) {
            sql = SELECT_FROM_FOR_MOVIE;
            id = movie.getId();
        }
        else if(hall != null) {
            sql = SELECT_FROM_FOR_HALL;
            id = hall.getId();
        }
        else {
            return list;
        }
        System.out.println(sql);
        try(Connection connection = CrudDAO.DATA_SOURCE.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            fillList(resultSet, list, movie, hall);
            resultSet.close();
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Fills list with new Performance objects created according to the rows of the ResultSet
     * @param resultSet - ResultSet object
     * @param list - list for adding new elements
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected void fillList(ResultSet resultSet, List<Performance> list) throws SQLException {
        fillList(resultSet, list, null, null);
    }

    /**
     * Fills list with new Performance objects created according to the rows of the ResultSet
     * @param resultSet - ResultSet object
     * @param list - list for adding new elements
     * @param statedMovie - reference for adding to each new element as value of the field
     * @param statedHall - reference for adding to each new element as value of the field
     * @throws SQLException - if a database error occurs
     */
    private void fillList(ResultSet resultSet, List<Performance> list, Movie statedMovie, Hall statedHall) throws SQLException {
        System.out.println("Performances for movie: " + statedMovie + "; for hall: " + statedHall);
        Map<Integer, Movie> movieMap = null;
        if(statedMovie == null) {
            movieMap = new HashMap<>();
        }
        Map<Integer, Hall> hallMap = null;
        if(statedHall == null) {
            hallMap = new HashMap<>();
        }

        while(resultSet.next()) {
            Performance performance = preparePerformance(resultSet);

            if(movieMap != null) {
                int movieId = resultSet.getInt("movie_id");
                Movie movie = movieMap.get(movieId);
                if(movie == null) {
                    movie = MovieDaoImpl.getInstance().readOne(resultSet);
                    movieMap.put(movieId, movie);
                }
                performance.setMovie(movie);
            }
            else {
                performance.setMovie(statedMovie);
            }

            if(hallMap != null) {
                int hallId = resultSet.getInt("hall_id");
                Hall hall = hallMap.get(hallId);
                if(hall == null) {
                    hall = HallDaoImpl.getInstance().readOne(resultSet);
                    hallMap.put(hallId, hall);
                }
                performance.setHall(hall);
            }
            else {
                performance.setHall(statedHall);
            }

            list.add(performance);
        }
    }

    /**
     * Returns new object of type Performance creating according to the row of the ResultSet
     * @param resultSet - ResultSet object
     * @return Performance object
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected Performance readOne(ResultSet resultSet) throws SQLException {
        Performance performance = preparePerformance(resultSet);
        performance.setMovie(MovieDaoImpl.getInstance().readOne(resultSet));
        performance.setHall(HallDaoImpl.getInstance().readOne(resultSet));
        return performance;
    }

    private Performance preparePerformance(ResultSet resultSet) throws SQLException {
        Performance performance = new Performance(){

            /**
             * Realization of Tickets list lazy initialization
             */
            private final Object locker = new Object();
            private List<Ticket> loadedTickets;

            @Override
            public List<Ticket> getTickets() {
                synchronized (locker) {
                    if(loadedTickets == null) {
                        loadedTickets = TicketDaoImpl.getInstance().selectFor(this);
                        System.out.println("Tickets list is loaded from DB for performance " + toString());
                    }
                    return loadedTickets;
                }
            }

            @Override
            public void setTickets(List<Ticket> tickets) {
                loadedTickets = tickets;
            }

        };
        performance.setId(resultSet.getInt("performance_id"));
        performance.setDate(resultSet.getDate("date").toLocalDate());
        performance.setTime(resultSet.getTime("time").toLocalTime());
        String prices = resultSet.getString("pricesForRows");
        Matcher matcher = PRICES_OF_THE_ROWS.matcher(prices);
        List<Integer> pricesForRows = new ArrayList<>();
        while(matcher.find()) {
            pricesForRows.add(Integer.valueOf(matcher.group("price")));
        }
        performance.setPricesForRows(pricesForRows);
        performance.setForSale(resultSet.getBoolean("forSale"));
        return performance;
    }

    /**
     * Creates new PreparedStatement object for adding new row to the database table
     * @param connection - Connection
     * @param element - object converting to the table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Performance element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        fill(preparedStatement, element);
        return preparedStatement;
    }

    /**
     * Creates new PreparedStatement object for updating row in the database table
     * @param connection - Connection
     * @param element - object converting to the new values of table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Performance element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        fill(preparedStatement, element);
        preparedStatement.setInt(7, element.getId());
        return preparedStatement;
    }

    private void fill(PreparedStatement preparedStatement, Performance element) throws SQLException {
        preparedStatement.setDate(1, Date.valueOf(element.getDate()));
        preparedStatement.setTime(2, Time.valueOf(element.getTime()));
        preparedStatement.setInt(3, element.getMovie().getId());
        preparedStatement.setInt(4, element.getHall().getId());
        StringBuilder stringBuilder = new StringBuilder();
        for(Integer priceForRow : element.getPricesForRows()) {
            stringBuilder.append(priceForRow).append("/");
        }
        preparedStatement.setString(5, stringBuilder.toString());
        preparedStatement.setBoolean(6, element.isForSale());
    }

}
