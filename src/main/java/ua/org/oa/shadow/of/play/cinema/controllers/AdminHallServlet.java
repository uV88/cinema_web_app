package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleHallDTO;
import ua.org.oa.shadow.of.play.cinema.checkers.HtmlReplacer;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodymyr on 23.03.17.
 *
 * Servlet deletes, updates and adds "hall" object.
 */
@WebServlet(name = "AdminHallServlet", urlPatterns = {"/delhall", "/purposehall", "/implhall"})
public class AdminHallServlet extends HttpServlet {

    private static final Pattern SEATS_NUMBER_PATTERN = Pattern.compile("(?<number>\\d+) *");

    /**
     * Method makes operation according to servlet path of request.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destination = request.getServletPath();
        switch (destination) {
            case "/delhall":
                deleteHall(request, response);
                break;
            case "/purposehall":
                prepareHall(request, response);
                break;
            case "/implhall":
                implementHall(request, response);
        }
    }

    /**
     * Deletes prepared for changing "hall" object from session.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String identification = request.getParameter("identification");
        request.getSession().removeAttribute("changedHallDTO" + identification);
        request.setAttribute("message", "Your choice is canceled");
        request.setAttribute("returnUrl", request.getContextPath() + "/hall");
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Deletes "hall" by it's key.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void deleteHall(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer hallId = Integer.valueOf(request.getParameter("hallId"));
        String message;
        String returnUrl;
        if(ServiceFactory.getSimpleHallDTOService().delete(hallId)) {
            message = "Hall has been successfully deleted!";
            returnUrl = request.getContextPath() + "/hall";
        }
        else {
            message = "Something went wrong!";
            returnUrl = request.getContextPath() + "/hall?id=" + hallId;
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Prepares "hall" for updating or creates template for new "hall", saves it in session.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void prepareHall(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SimpleHallDTO simpleHallDTO;
        String hallId = request.getParameter("hallId");
        if(hallId != null) {
            /*
             * admin chose hall to edit
             */
            Integer id = Integer.valueOf(hallId);
            simpleHallDTO = ServiceFactory.getSimpleHallDTOService().getById(id);
            //to prevent NullPointerException:
            if(simpleHallDTO == null) {
                request.setAttribute("message", "Hall doesn't exists!");
                request.setAttribute("returnUrl", request.getContextPath() + "/hall");
                request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
                return;
            }
        }
        else {
            /*
             * admin wants to add new hall
             */
            simpleHallDTO = new SimpleHallDTO();
            simpleHallDTO.setId(-1);
        }
        request.setAttribute("changedHallDTO", simpleHallDTO);
        String identification = String.valueOf(System.currentTimeMillis());
        request.setAttribute("identification", identification);
        request.getSession().setAttribute("changedHallDTO" + identification, simpleHallDTO);
        request.getRequestDispatcher("pages/admin/chHall.jsp").forward(request, response);
    }

    /**
     * Adds changes to saved in session "hall", updates "hall" in DB or adds new one.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void implementHall(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String identification = request.getParameter("identification");
        SimpleHallDTO simpleHallDTO = (SimpleHallDTO) request.getSession().getAttribute("changedHallDTO" + identification);
        if(simpleHallDTO == null) {
            request.setAttribute("message", "Your request is ignored!");
            request.setAttribute("returnUrl", request.getContextPath() + "/hall");
            request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
            return;
        }
        request.getSession().removeAttribute("changedHallDTO" + identification);

        simpleHallDTO.setHallName(HtmlReplacer.replaceChars(request.getParameter("name")));

        String message;
        String returnUrl;
        if(simpleHallDTO.getId() < 0) {
            /*
             * it's possible to set number of seats (of each row and total)
             * only at the moment of saving new hall
             */
            Matcher matcher = SEATS_NUMBER_PATTERN.matcher(request.getParameter("seats"));
            List<Integer> seatsInRows = new ArrayList<>();
            int seatsNumber = 0;
            while(matcher.find()) {
                int seatsInOneRow = Integer.parseInt(matcher.group("number"));
                seatsInRows.add(seatsInOneRow);
                seatsNumber += seatsInOneRow;
            }
            simpleHallDTO.setSeatsScheme(seatsInRows);
            simpleHallDTO.setSeatsNumber(seatsNumber);
            /*
             * saving
             */
            ServiceFactory.getSimpleHallDTOService().save(simpleHallDTO);
            if(simpleHallDTO.getId() > 0) {
                message = "Hall has been successfully added!";
                returnUrl = request.getContextPath() + "/hall?id=" + simpleHallDTO.getId();
            }
            else {
                message = "Something went wrong!";
                returnUrl = request.getContextPath() + "/hall";
            }
        }
        else {
            /*
             * updating
             */
            if(ServiceFactory.getSimpleHallDTOService().update(simpleHallDTO)) {
                message = "Hall has been successfully updated!";
            }
            else {
                message = "Something went wrong!";
            }
            returnUrl = request.getContextPath() + "/hall?id=" + simpleHallDTO.getId();
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

}
