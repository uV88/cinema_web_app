package ua.org.oa.shadow.of.play.cinema.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import ua.org.oa.shadow.of.play.cinema.helper.PropertyHolder;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Methods of class create and initiate pool of connections to relative DB.
 * Calling appropriate method returns connection to DB.
 */
public final class DataSource {

    private static final DataSource dataSource = new DataSource();

    private ComboPooledDataSource poolConnections;

    /**
     * Creates new DataSource object
     */
    private DataSource() {
        initPoolConnections();
    }

    /**
     * Returns sole object of this class
     * @return DataSource object
     */
    public static DataSource getInstance() {
        return dataSource;
    }

    /**
     * Returns connection from the pool
     * @return Connection object
     */
    public Connection getConnection() throws SQLException {
        return poolConnections.getConnection();
    }

    /**
     * Initializes pool of connections
     */
    private void initPoolConnections() {
        poolConnections = new ComboPooledDataSource();
        PropertyHolder propertyHolder = PropertyHolder.getInstance();
        try {
            poolConnections.setDriverClass(propertyHolder.getDbDriver());
        }
        catch (PropertyVetoException e) {
            /* can't work without driver */
            throw new Error(e);
        }
            poolConnections.setJdbcUrl(propertyHolder.getJdbcUrl());
            poolConnections.setUser(propertyHolder.getDbUserLogin());
            poolConnections.setPassword(propertyHolder.getDbUserPassword());

            poolConnections.setMinPoolSize(propertyHolder.getMinPoolSize());
            poolConnections.setAcquireIncrement(propertyHolder.getAcquireIncrement());
            poolConnections.setMaxPoolSize(propertyHolder.getMaxPoolSize());
    }

}
