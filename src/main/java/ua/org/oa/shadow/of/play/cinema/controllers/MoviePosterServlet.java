package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleMovieDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodymyr on 03.04.17.
 *
 * Servlet edits "movie": saves image and adds proper URL to "movie" as movie poster URL.
 */
@WebServlet(name = "MoviePosterServlet", urlPatterns = "/addposter")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
        maxFileSize=1024*1024*10,      // 10MB
        maxRequestSize=1024*1024*50,    // 50MB
        location="/opt/tomcat/img")
public class MoviePosterServlet extends HttpServlet {

    private static final String CHARSET_NAME = "UTF-8";
    private static final Pattern FILENAME_PATTERN = Pattern.compile(".+(?<extension>\\..+)");

    /**
     * Saves image and adds proper URL to "movie" as movie poster URL.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String movieIdString;
        Part movieIdPart = request.getPart("movieId");
        try(BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(movieIdPart.getInputStream(), CHARSET_NAME))) {
            movieIdString = bufferedReader.readLine();
        }

        String message;
        Part filePart = request.getPart("file");
        if(filePart.getSize() > 0) {

            String fileName;
            Matcher matcher = FILENAME_PATTERN.matcher(filePart.getSubmittedFileName());
            if (matcher.matches()) {
                //found extension (for example, ".jpg")
                fileName = movieIdString + matcher.group("extension");
            } else {
                //no extension
                fileName = movieIdString;
            }
            filePart.write(fileName);

            Integer movieId = Integer.valueOf(movieIdString);
            SimpleMovieDTO simpleMovieDTO = ServiceFactory.getSimpleMovieDTOService().getById(movieId);
            simpleMovieDTO.setPosterUrl("/img/" + fileName);
            if (ServiceFactory.getSimpleMovieDTOService().update(simpleMovieDTO)) {
                message = "Poster successfully added to \"" + simpleMovieDTO.getMovieTitle() + "\".";
            } else {
                message = "Something went wrong!";
            }
        }
        else {
            message = "Select image for poster!";
        }
        String returnUrl = request.getContextPath() + "/movie?id=" + movieIdString;
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Ignores request, sends redirect to observe list of "movies".
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/movie");
    }
}
