package ua.org.oa.shadow.of.play.cinema.dao.impl;

import ua.org.oa.shadow.of.play.cinema.model.Hall;
import ua.org.oa.shadow.of.play.cinema.model.Performance;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class implements Dao interface for keeping objects of class Hall in data storage.
 */
public final class HallDaoImpl extends CrudDAO<Hall> {

    private static final Pattern NUMBER_OF_SEATS_IN_THE_ROWS = Pattern.compile("(?<number>\\d+)\\/*");
    private static final HallDaoImpl crudDAO = new HallDaoImpl();
    private static final String SELECT_FROM = "SELECT * FROM Hall";
    private static final String ORDER_BY = "ORDER BY hallName";

    private final String INSERT = "INSERT INTO Hall (hallName, seatsNumber, seatsScheme) VALUES (?, ?, ?)";
    private final String UPDATE = "UPDATE Hall SET hallName=?, seatsNumber=?, seatsScheme=? WHERE hall_id=?";

    /**
     * Creates new MovieDaoImpl object
     */
    private HallDaoImpl() {
        super(SELECT_FROM, ORDER_BY, Hall.class);
    }

    /**
     * Returns sole object of this class
     * @return HallDaoImpl object
     */
    public static HallDaoImpl getInstance() {
        return crudDAO;
    }

    /**
     * Returns new object of type Hall creating according to the row of the ResultSet
     * @param resultSet - ResultSet object
     * @return Hall object
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected Hall readOne(ResultSet resultSet) throws SQLException {
        Hall hall = new Hall(){

            /**
             * Realization of Performances list lazy initialization
             */
            private final Object locker = new Object();
            private List<Performance> loadedPerformances;

            @Override
            public List<Performance> getPerformances() {
                synchronized (locker) {
                    if(loadedPerformances == null) {
                        loadedPerformances = PerformanceDaoImpl.getInstance().selectFor(this);
                        System.out.println("Performances list is loaded from DB for hall " + toString());
                    }
                    return loadedPerformances;
                }
            }

            @Override
            public void setPerformances(List<Performance> performances) {
                loadedPerformances = performances;
            }
        };
        hall.setId(resultSet.getInt("hall_id"));
        hall.setHallName(resultSet.getString("hallName"));
        hall.setSeatsNumber(resultSet.getInt("seatsNumber"));
        String seats = resultSet.getString("seatsScheme");
        Matcher matcher = NUMBER_OF_SEATS_IN_THE_ROWS.matcher(seats);
        List<Integer> seatsInRowsNumber = new ArrayList<>();
        while(matcher.find()) {
            seatsInRowsNumber.add(Integer.valueOf(matcher.group("number")));
        }
        hall.setSeatsScheme(seatsInRowsNumber);
        return hall;
    }

    /**
     * Creates new PreparedStatement object for adding new row to the database table
     * @param connection - Connection
     * @param element - object converting to the table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Hall element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        fill(preparedStatement, element);
        return preparedStatement;
    }

    /**
     * Creates new PreparedStatement object for updating row in the database table
     * @param connection - Connection
     * @param element - object converting to the new values of table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        fill(preparedStatement, element);
        preparedStatement.setInt(4, element.getId());
        return preparedStatement;
    }

    private void fill(PreparedStatement preparedStatement, Hall element) throws SQLException {
        preparedStatement.setString(1, element.getHallName());
        preparedStatement.setInt(2, element.getSeatsNumber());
        StringBuilder seats = new StringBuilder();
        for(Integer seatsInRowNumber : element.getSeatsScheme()) {
            seats.append(seatsInRowNumber).append("/");
        }
        preparedStatement.setString(3, seats.toString());
    }

}
