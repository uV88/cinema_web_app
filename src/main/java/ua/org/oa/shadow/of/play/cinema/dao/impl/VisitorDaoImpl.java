package ua.org.oa.shadow.of.play.cinema.dao.impl;

import ua.org.oa.shadow.of.play.cinema.dao.api.VisitorDao;
import ua.org.oa.shadow.of.play.cinema.model.Ticket;
import ua.org.oa.shadow.of.play.cinema.model.Visitor;

import java.sql.*;
import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class implements VisitorDao interface for keeping objects of class Visitor in data storage.
 */
public final class VisitorDaoImpl extends CrudDAO<Visitor> implements VisitorDao<Integer, Visitor> {

    private static final VisitorDaoImpl crudDAO = new VisitorDaoImpl();
    private static final String SELECT_FROM = "SELECT * FROM Visitor";

    private final String SELECT_BY_LOGIN = "SELECT * FROM Visitor WHERE login=?";
    private final String INSERT = "INSERT INTO Visitor (login, firstName, lastName, emailAddress, password, role, " +
            "blocked) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "UPDATE Visitor SET login=?, firstName=?, lastName=?, emailAddress=?, password=?, " +
            "role=?, blocked=? WHERE visitor_id=?";

    /**
     * Creates new MovieDaoImpl object
     */
    private VisitorDaoImpl() {
        super(SELECT_FROM, Visitor.class);
    }

    /**
     * Returns sole object of this class
     * @return VisitorDaoImpl object
     */
    public static VisitorDaoImpl getInstance() {
        return crudDAO;
    }

    /**
     * Returns Visitor by it's login
     * @param login - "login"
     * @return Visitor object
     */
    @Override
    public Visitor getByLogin(String login) {
        System.out.println(SELECT_BY_LOGIN);
        Visitor visitor = null;
        try(Connection connection = CrudDAO.DATA_SOURCE.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_LOGIN)){
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                visitor = readOne(resultSet);
            }
            resultSet.close();
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        return visitor;
    }

    /**
     * Returns new object of type Visitor creating according to the row of the ResultSet
     * @param resultSet - ResultSet object
     * @return Visitor object
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected Visitor readOne(ResultSet resultSet) throws SQLException {
        Visitor visitor = new Visitor(){

            /**
             * Realization of Tickets list lazy initialization
             */
            private final Object locker = new Object();
            private List<Ticket> loadedTickets;

            @Override
            public List<Ticket> getTickets() {
                synchronized (locker) {
                    if(loadedTickets == null) {
                        loadedTickets = TicketDaoImpl.getInstance().selectFor(this);
                        System.out.println("Tickets list is loaded from DB for visitor " + toString());
                    }
                    return loadedTickets;
                }
            }

            @Override
            public void setTickets(List<Ticket> tickets) {
                loadedTickets = tickets;
            }

        };
        visitor.setId(resultSet.getInt("visitor_id"));
        visitor.setLogin(resultSet.getString("login"));
        visitor.setFirstName(resultSet.getString("firstName"));
        visitor.setLastName(resultSet.getString("lastName"));
        visitor.setEmailAddress(resultSet.getString("emailAddress"));
        visitor.setPassword(resultSet.getString("password"));
        visitor.setRole(Visitor.Role.valueOf(resultSet.getString("role")));
        visitor.setBlocked(resultSet.getBoolean("blocked"));
        return visitor;
    }

    /**
     * Creates new PreparedStatement object for adding new row to the database table
     * @param connection - Connection
     * @param element - object converting to the table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Visitor element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        fill(preparedStatement, element);
        return preparedStatement;
    }

    /**
     * Creates new PreparedStatement object for updating row in the database table
     * @param connection - Connection
     * @param element - object converting to the new values of table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Visitor element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        fill(preparedStatement, element);
        preparedStatement.setInt(8, element.getId());
        return preparedStatement;
    }

    private void fill(PreparedStatement preparedStatement, Visitor element) throws SQLException {
        preparedStatement.setString(1, element.getLogin());
        preparedStatement.setString(2, element.getFirstName());
        preparedStatement.setString(3, element.getLastName());
        preparedStatement.setString(4, element.getEmailAddress());
        preparedStatement.setString(5, element.getPassword());
        preparedStatement.setString(6, element.getRole().toString());
        preparedStatement.setBoolean(7, element.isBlocked());
    }
}
