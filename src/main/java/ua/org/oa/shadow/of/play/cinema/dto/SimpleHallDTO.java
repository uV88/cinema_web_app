package ua.org.oa.shadow.of.play.cinema.dto;

import ua.org.oa.shadow.of.play.cinema.model.Entity;

import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class represents cinema hall (without list of performances).
 */
public class SimpleHallDTO extends Entity {

    private String hallName;
    private int seatsNumber;
    private List<Integer> seatsScheme;

    /**
     * Creates new SimpleHallDTO object
     */
    public SimpleHallDTO() {
    }

    /**
     * Creates new SimpleHallDTO object
     * @param hallName - name of the hall
     * @param seatsNumber - number of seats in the hall
     * @param seatsScheme - seats disposition in the hall
     */
    public SimpleHallDTO(String hallName, int seatsNumber, List<Integer> seatsScheme) {
        this.hallName = hallName;
        this.seatsNumber = seatsNumber;
        this.seatsScheme = seatsScheme;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public List<Integer> getSeatsScheme() {
        return seatsScheme;
    }

    public void setSeatsScheme(List<Integer> seatsScheme) {
        this.seatsScheme = seatsScheme;
    }

    @Override
    public String toString() {
        return "SimpleHallDTO{" +
                "id=" + getId() +
                ", hallName='" + hallName + '\'' +
                ", seatsNumber=" + seatsNumber +
                '}';
    }
}
