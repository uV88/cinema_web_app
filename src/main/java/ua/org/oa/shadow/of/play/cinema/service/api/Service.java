package ua.org.oa.shadow.of.play.cinema.service.api;

import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * This interface sets base functionality of data storage.
 * It's implying that unique key value binds with each element in data storage
 * and each element can save his unique key as field value.
 */
public interface Service<K, V> {

    /**
     * Returns all elements
     * @return all elements
     */
    List<V> getAll();

    /**
     * Returns element by it's key
     * @param key - key
     * @return element according to the key
     */
    V getById(K key);

    /**
     * Saves element
     * @param element - object for saving
     */
    void save(V element);

    /**
     * Deletes element by it's key
     * @param key - key
     * @return 'true' if element with this key was successfully deleted, 'false' otherwise
     */
    boolean delete(K key);

    /**
     * Updates element
     * @param element - object for updating
     * @return 'true' if element with the same key was successfully updated, 'false' otherwise
     */
    boolean update(V element);

}
