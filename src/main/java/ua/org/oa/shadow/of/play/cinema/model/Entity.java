package ua.org.oa.shadow.of.play.cinema.model;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Objects of this class and all it's subclasses have integer "id".
 */
public class Entity {

    private int id;

    /**
     * Creates new Entity object
     */
    public Entity() {
    }

    /**
     * Creates new Entity object
     * @param id - Entity object identification
     */
    Entity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
