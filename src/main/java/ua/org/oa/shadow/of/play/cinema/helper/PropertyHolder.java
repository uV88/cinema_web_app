package ua.org.oa.shadow.of.play.cinema.helper;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class provides reading properties from the file.
 * Calling appropriate methods returns these properties.
 */
public final class PropertyHolder {

    private static final PropertyHolder propertyHolder = new PropertyHolder();

    private boolean isInMemoryDB;
    private String inMemoryDBAdminLogin;
    private String inMemoryDBAdminPassword;
    private String inMemoryDBUserLogin;
    private String inMemoryDBUserPassword;
    private String jdbcUrl;
    private String dbUserLogin;
    private String dbUserPassword;
    private String dbDriver;
    private int minPoolSize;
    private int acquireIncrement;
    private int maxPoolSize;

    /**
     * Creates new PropertyHolder object
     */
    private PropertyHolder() {
        try {
            Properties properties = new Properties();
            properties.load(PropertyHolder.class.getClassLoader().getResourceAsStream("webApp.properties"));
            this.isInMemoryDB = Boolean.valueOf(properties.getProperty("isInMemoryDB"));
            this.inMemoryDBAdminLogin = properties.getProperty("inMemoryDBAdminLogin");
            this.inMemoryDBAdminPassword = properties.getProperty("inMemoryDBAdminPassword");
            this.inMemoryDBUserLogin = properties.getProperty("inMemoryDBUserLogin");
            this.inMemoryDBUserPassword = properties.getProperty("inMemoryDBUserPassword");
            this.jdbcUrl = properties.getProperty("jdbcUrl");
            this.dbUserLogin = properties.getProperty("dbUserLogin");
            this.dbUserPassword = properties.getProperty("dbUserPassword");
            this.dbDriver = properties.getProperty("dbDriver");
            this.minPoolSize = Integer.valueOf(properties.getProperty("minPoolSize"));
            this.acquireIncrement = Integer.valueOf(properties.getProperty("acquireIncrement"));
            this.maxPoolSize = Integer.valueOf(properties.getProperty("maxPoolSize"));
        }
        catch (IOException e) {
            /* can't work without these properties */
            throw new Error(e);
        }    }

    /**
     * Returns sole object of this class
     * @return PropertiesHolder object
     */
    public static PropertyHolder getInstance() {
        return propertyHolder;
    }

    public boolean isInMemoryDB() {
        return isInMemoryDB;
    }

    public String getInMemoryDBAdminLogin() {
        return inMemoryDBAdminLogin;
    }

    public String getInMemoryDBAdminPassword() {
        return inMemoryDBAdminPassword;
    }

    public String getInMemoryDBUserLogin() {
        return inMemoryDBUserLogin;
    }

    public String getInMemoryDBUserPassword() {
        return inMemoryDBUserPassword;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public String getDbUserLogin() {
        return dbUserLogin;
    }

    public String getDbUserPassword() {
        return dbUserPassword;
    }

    public String getDbDriver() {
        return dbDriver;
    }

    public int getMinPoolSize() {
        return minPoolSize;
    }

    public int getAcquireIncrement() {
        return acquireIncrement;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

}
