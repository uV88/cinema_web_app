package ua.org.oa.shadow.of.play.cinema.model;

import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class represents movie.
 */
public class Movie extends Entity {

    private String movieTitle;
    private String originalMovieTitle;
    private String posterUrl;
    private String movieDescription;
    private String director;
    private String screenwriter;
    private String actors;
    private List<Performance> performances;

    /**
     * Creates new Movie object
     */
    public Movie() {
    }

    /**
     * Creates new Movie object
     * @param movieTitle - movie title
     * @param movieDescription - movie description
     */
    public Movie(String movieTitle, String movieDescription) {
        this.movieTitle = movieTitle;
        this.movieDescription = movieDescription;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getOriginalMovieTitle() {
        return originalMovieTitle;
    }

    public void setOriginalMovieTitle(String originalMovieTitle) {
        this.originalMovieTitle = originalMovieTitle;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getScreenwriter() {
        return screenwriter;
    }

    public void setScreenwriter(String screenwriter) {
        this.screenwriter = screenwriter;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + getId() +
                ", movieTitle='" + movieTitle + '\'' +
                ", originalMovieTitle='" + originalMovieTitle + '\'' +
                ", director='" + director + '\'' +
                ", screenwriter='" + screenwriter + '\'' +
                ", actors='" + actors + '\'' +
                '}';
    }
}

