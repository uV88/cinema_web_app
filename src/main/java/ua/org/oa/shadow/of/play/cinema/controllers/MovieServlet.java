package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.MovieDTO;
import ua.org.oa.shadow.of.play.cinema.dto.SimpleMovieDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by volodymyr on 19.03.17.
 *
 * Servlet prepares single "movie" or list of "movies" for observing.
 */
@WebServlet(name = "MovieServlet", urlPatterns = {"/movie"})
public class MovieServlet extends HttpServlet {

    /**
     * Calls doGet(request, response).
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Prepares single "movie" or list of "movies" for observing.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String movieIdStr = request.getParameter("id");
        if(movieIdStr != null && movieIdStr.length() != 0) {
            Integer movieId = Integer.valueOf(movieIdStr);
            MovieDTO movieDTO = ServiceFactory.getMovieDTOService().getById(movieId);
            if(movieDTO != null) {
                request.setAttribute("movieDTO", movieDTO);
                request.getRequestDispatcher("pages/common/movie.jsp").forward(request, response);
            }
            else {
                //movie with this 'id' does not exist
                response.sendRedirect(request.getContextPath() + "/movie");
            }
        }
        else {
            List<SimpleMovieDTO> simpleMovieDTOList = ServiceFactory.getSimpleMovieDTOService().getAll();
            request.setAttribute("simpleMovieDTOList", simpleMovieDTOList);
            request.getRequestDispatcher("pages/common/movies.jsp").forward(request, response);
        }
    }
}
