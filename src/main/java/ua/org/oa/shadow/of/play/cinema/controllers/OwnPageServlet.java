package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;
import ua.org.oa.shadow.of.play.cinema.dto.VisitorDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 02.04.17.
 *
 * Servlet prepares authorized visitor own profile for observing.
 */
@WebServlet(name = "OwnPageServlet", urlPatterns = "/me")
public class OwnPageServlet extends HttpServlet {

    /**
     * Calls doGet(request, response).
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Prepares authorized visitor own profile.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SimpleVisitorDTO simpleVisitorDTO = (SimpleVisitorDTO) request.getSession().getAttribute("visitor");
        VisitorDTO visitorDTO = ServiceFactory.getVisitorDTOService().getById(simpleVisitorDTO.getId());
        request.setAttribute("visitorDTO", visitorDTO);
        request.getRequestDispatcher("pages/common/visitor.jsp").forward(request, response);
    }
}
