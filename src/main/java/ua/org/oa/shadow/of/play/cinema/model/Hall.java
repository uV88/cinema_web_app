package ua.org.oa.shadow.of.play.cinema.model;

import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class represents cinema hall.
 */
public class Hall extends Entity {

    private String hallName;
    private int seatsNumber;
    private List<Integer> seatsScheme;
    private List<Performance> performances;

    /**
     * Creates new Hall object
     */
    public Hall() {
    }

    /**
     * Creates new Hall object
     * @param hallName - name of the hall
     * @param seatsNumber - number of seats in the hall
     * @param seatsScheme - seats disposition in the hall
     */
    public Hall(String hallName, int seatsNumber, List<Integer> seatsScheme) {
        this.hallName = hallName;
        this.seatsNumber = seatsNumber;
        this.seatsScheme = seatsScheme;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public List<Integer> getSeatsScheme() {
        return seatsScheme;
    }

    public void setSeatsScheme(List<Integer> seatsScheme) {
        this.seatsScheme = seatsScheme;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "id=" + getId() +
                ", hallName='" + hallName + '\'' +
                ", seatsNumber=" + seatsNumber +
                '}';
    }
}
