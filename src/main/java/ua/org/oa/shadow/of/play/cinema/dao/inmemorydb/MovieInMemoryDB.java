package ua.org.oa.shadow.of.play.cinema.dao.inmemorydb;

import ua.org.oa.shadow.of.play.cinema.model.Movie;
import ua.org.oa.shadow.of.play.cinema.model.Performance;

import java.util.ArrayList;

/**
 * Created by volodymyr on 04.04.17.
 *
 * Class implements Dao interface for keeping objects of class Movie in RAM data storage.
 * Overridden methods of this class make behaviour of memory DB similar to behaviour of relative DB.
 */
public final class MovieInMemoryDB extends InMemoryDB<Movie> {

    private static final MovieInMemoryDB movieInMemoryDB = new MovieInMemoryDB();

    /**
     * Creates new MovieInMemoryDB object
     */
    private MovieInMemoryDB() {
    }

    /**
     * Returns sole object of this class
     * @return MovieInMemoryDB object
     */
    public static MovieInMemoryDB getInstance() {
        return movieInMemoryDB;
    }

    /**
     * Adds new Movie object to this storage
     * @param movie - object for adding
     */
    @Override
    public void save(Movie movie) {
        synchronized(InMemoryDB.LOCKER) {
            movie.setPerformances(new ArrayList<>());
            super.save(movie);
        }
    }

    /**
     * Replaces Movie object by key with new object
     * @param movie - new object
     * @return 'true' if element with the same key was successfully updated, 'false' otherwise
     */
    @Override
    public boolean update(Movie movie) {
        synchronized(InMemoryDB.LOCKER) {
            Movie oldMovie = replaceWith(movie);
            if (oldMovie != null) {
                //(Movie with the same key was in DB)

                movie.setPerformances(oldMovie.getPerformances());
                for (Performance performance : movie.getPerformances()) {
                    performance.setMovie(movie);
                }
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Deletes key-value pair
     * @param key - key
     * @return 'true' if pair with this key was successfully deleted, 'false' otherwise
     */
    @Override
    public boolean delete(Integer key) {
        synchronized(InMemoryDB.LOCKER) {
            Movie movie = getById(key);
            return movie != null && !(movie.getPerformances().size() > 0) && super.delete(key);
        }
    }

}
