package ua.org.oa.shadow.of.play.cinema.checkers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by volodymyr on 24.03.17.
 *
 * Single static method of this class provides replacing reserved chars (used in HTML documents)
 * with special HTML symbols
 */
public final class HtmlReplacer {

    private static final Pattern SPECIAL_CHARS_PATTERN =
            Pattern.compile("[&\"'<>]");
    private static final String[][] SPECIAL_CHARS =
            {{"&", "&amp;"}, {"\"", "&quot;"}, {"\'", "&apos;"}, {"<", "&lt;"}, {">", "&gt;"}};

    /**
     * Replaces all reserved chars with special HTML symbols
     * @param htmlString - string of HTML text
     * @return string having replaced reserved chars
     */
    public static String replaceChars(String htmlString) {
        StringBuffer stringBuffer = new StringBuffer(htmlString.length());
        Matcher matcher = SPECIAL_CHARS_PATTERN.matcher(htmlString);
        while(matcher.find()) {
            String reservedChar = matcher.group();
            for(String[] charEntry : SPECIAL_CHARS) {
                if(charEntry[0].equals(reservedChar)) {
                    matcher.appendReplacement(stringBuffer, charEntry[1]);
                }
            }
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }

}
