package ua.org.oa.shadow.of.play.cinema.dao.inmemorydb;

import ua.org.oa.shadow.of.play.cinema.model.Hall;
import ua.org.oa.shadow.of.play.cinema.model.Movie;
import ua.org.oa.shadow.of.play.cinema.model.Performance;
import ua.org.oa.shadow.of.play.cinema.model.Ticket;

import java.util.ArrayList;

/**
 * Created by volodymyr on 03.04.17.
 *
 * Class implements Dao interface for keeping objects of class Performance in RAM data storage.
 * Overridden methods of this class make behaviour of memory DB similar to behaviour of relative DB.
 */
public final class PerformanceInMemoryDB extends InMemoryDB<Performance> {

    private static final PerformanceInMemoryDB performanceInMemoryDB = new PerformanceInMemoryDB();

    /**
     * Creates new PerformanceInMemoryDB object
     */
    private PerformanceInMemoryDB() {
    }

    /**
     * Returns sole object of this class
     * @return PerformanceInMemoryDB object
     */
    public static PerformanceInMemoryDB getInstance() {
        return performanceInMemoryDB;
    }

    /**
     * Adds new Performance to this storage
     * @param performance - object for adding
     */
    @Override
    public void save(Performance performance) {
        synchronized(InMemoryDB.LOCKER) {
            Movie movie = MovieInMemoryDB.getInstance().getById(performance.getMovie().getId());
            Hall hall = HallInMemoryDB.getInstance().getById(performance.getHall().getId());
            if (movie != null && hall != null) {
                //(Movie and Hall are already in DB)
                performance.setMovie(movie);
                performance.setHall(hall);
                performance.setTickets(new ArrayList<>());
                super.save(performance);
                //add this Performance to the performances list of Movie:
                performance.getMovie().getPerformances().add(performance);
                //add this Performance to the performances list of Hall
                performance.getHall().getPerformances().add(performance);
            }
        }
    }

    /**
     * Replaces Performance object by key with new object
     * @param performance - new object
     * @return 'true' if element with the same key was successfully updated, 'false' otherwise
     */
    @Override
    public boolean update(Performance performance) {
        synchronized(InMemoryDB.LOCKER) {
            Movie movie = MovieInMemoryDB.getInstance().getById(performance.getMovie().getId());
            Hall hall = HallInMemoryDB.getInstance().getById(performance.getHall().getId());
            if (movie != null && hall != null) {
                //(Movie and Hall are already in DB)
                performance.setMovie(movie);
                performance.setHall(hall);
                Performance oldPerformance = replaceWith(performance);
                if (oldPerformance != null) {
                    //(Performance with the same key was in DB)

                    oldPerformance.getMovie().getPerformances().remove(oldPerformance);
                    performance.getMovie().getPerformances().add(performance);

                    oldPerformance.getHall().getPerformances().remove(oldPerformance);
                    performance.getHall().getPerformances().add(performance);

                    performance.setTickets(oldPerformance.getTickets());
                    for (Ticket ticket : performance.getTickets()) {
                        ticket.setPerformance(performance);
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Deletes key-value pair
     * @param key - key
     * @return 'true' if pair with this key was successfully deleted, 'false' otherwise
     */
    @Override
    public boolean delete(Integer key) {
        synchronized(InMemoryDB.LOCKER) {
            Performance performance = getById(key);
            if (performance != null) {
                if (performance.getTickets().size() > 0) {
                    //(There are sold tickets for this performance)
                    return false;
                } else {
                    //delete this Performance from the performances list of Movie:
                    performance.getMovie().getPerformances().remove(performance);
                    //delete this Performance from the performances list of Hall:
                    performance.getHall().getPerformances().remove(performance);
                    return super.delete(key);
                }
            } else {
                return false;
            }
        }
    }

}
