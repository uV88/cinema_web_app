package ua.org.oa.shadow.of.play.cinema.dao.impl;

import ua.org.oa.shadow.of.play.cinema.model.Movie;
import ua.org.oa.shadow.of.play.cinema.model.Performance;

import java.sql.*;
import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class implements Dao interface for keeping objects of class Movie in data storage.
 */
public final class MovieDaoImpl extends CrudDAO<Movie> {

    private static final MovieDaoImpl crudDAO = new MovieDaoImpl();
    private static final String SELECT_FROM = "SELECT * FROM Movie";

    private final String INSERT = "INSERT INTO Movie (movieTitle, originalMovieTitle, movieDescription," +
            " posterUrl, director, screenwriter, actors) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "UPDATE Movie SET movieTitle=?, originalMovieTitle=?, movieDescription=?," +
            " posterUrl=?, director=?, screenwriter=?, actors=? WHERE movie_id=?";

    /**
     * Creates new MovieDaoImpl object
     */
    private MovieDaoImpl() {
        super(SELECT_FROM, Movie.class);
    }

    /**
     * Returns sole object of this class
     * @return MovieDaoImpl object
     */
    public static MovieDaoImpl getInstance() {
        return crudDAO;
    }

    /**
     * Returns new object of type Movie creating according to the row of the ResultSet
     * @param resultSet - ResultSet object
     * @return Movie object
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected Movie readOne(ResultSet resultSet) throws SQLException {
        Movie movie = new Movie(){

            /**
             * Realization of Performances list lazy initialization
             */
            private final Object locker = new Object();
            private List<Performance> loadedPerformances;

            @Override
            public List<Performance> getPerformances() {
                synchronized (locker) {
                    if(loadedPerformances == null) {
                        loadedPerformances = PerformanceDaoImpl.getInstance().selectFor(this);
                        System.out.println("Performances list is loaded from DB for movie " + toString());
                    }
                    return loadedPerformances;
                }
            }

            @Override
            public void setPerformances(List<Performance> performances) {
                loadedPerformances = performances;
            }

        };
        movie.setId(resultSet.getInt("movie_id"));
        movie.setMovieTitle(resultSet.getString("movieTitle"));
        movie.setOriginalMovieTitle(resultSet.getString("originalMovieTitle"));
        movie.setMovieDescription(resultSet.getString("movieDescription"));
        movie.setPosterUrl(resultSet.getString("posterUrl"));
        movie.setDirector(resultSet.getString("director"));
        movie.setScreenwriter(resultSet.getString("screenwriter"));
        movie.setActors(resultSet.getString("actors"));
        return movie;
    }

    /**
     * Creates new PreparedStatement object for adding new row to the database table
     * @param connection - Connection
     * @param element - object converting to the table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Movie element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        fill(preparedStatement, element);
        return preparedStatement;
    }

    /**
     * Creates new PreparedStatement object for updating row in the database table
     * @param connection - Connection
     * @param element - object converting to the new values of table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Movie element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        fill(preparedStatement, element);
        preparedStatement.setInt(8, element.getId());
        return preparedStatement;
    }

    private void fill(PreparedStatement preparedStatement, Movie element) throws SQLException {
        preparedStatement.setString(1, element.getMovieTitle());
        preparedStatement.setString(2, element.getOriginalMovieTitle());
        preparedStatement.setString(3, element.getMovieDescription());
        preparedStatement.setString(4, element.getPosterUrl());
        preparedStatement.setString(5, element.getDirector());
        preparedStatement.setString(6, element.getScreenwriter());
        preparedStatement.setString(7, element.getActors());
    }
}
