package ua.org.oa.shadow.of.play.cinema.mapper;

import org.dozer.DozerBeanMapper;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Methods of this class create object of stated type according to stated object of other type.
 */
public final class BeanMapper {

    private static final BeanMapper beanMapper = new BeanMapper();
    private DozerBeanMapper mapper;

    /**
     * Creates new BeanMapper object
     */
    private BeanMapper() {
        mapper = new DozerBeanMapper();
        List<String> mappingFiles = new ArrayList<>();
        URL convertersUrl = BeanMapper.class.getClassLoader().getResource("dozerJdk8Converters.xml");
        if(convertersUrl != null) {
            mappingFiles.add(convertersUrl.toString());
        }
        URL mappingUrl = BeanMapper.class.getClassLoader().getResource("doserMapping.xml");
        if(mappingUrl != null) {
            mappingFiles.add(mappingUrl.toString());
        }
        mapper.setMappingFiles(mappingFiles);
    }

    /**
     * Returns sole object of this class
     * @return BeanMapper object
     */
    public static BeanMapper getInstance() {
        return beanMapper;
    }

    /**
     * Map object to another class
     * @param from - object for mapping
     * @param toClass - destination class
     * @param <T> - destination type
     * @return object of type T
     */
    public <T> T singleMapper(Object from, Class<T> toClass) {
        return mapper.map(from, toClass);
    }

    /**
     * Map each object of the collection to another class
     * @param iterable - collection of objects
     * @param toClass - destination class
     * @param <E> - type of objects of the collection
     * @param <T> - destination type
     * @return list of objects of type T
     */
    public <E, T> List<T> listMapToList(Iterable<E> iterable, Class<T> toClass) {
        List<T> list = new ArrayList<>();
        for(E e : iterable) {
            list.add(mapper.map(e, toClass));
        }
        return list;
    }

}
