package ua.org.oa.shadow.of.play.cinema.dao.inmemorydb;

import ua.org.oa.shadow.of.play.cinema.model.Hall;
import ua.org.oa.shadow.of.play.cinema.model.Performance;

import java.util.ArrayList;

/**
 * Created by volodymyr on 04.04.17.
 *
 * Class implements Dao interface for keeping objects of class Hall in RAM data storage.
 * Overridden methods of this class make behaviour of memory DB similar to behaviour of relative DB.
 */
public final class HallInMemoryDB extends InMemoryDB<Hall> {

    private static final HallInMemoryDB hallInMemoryDB = new HallInMemoryDB();

    /**
     * Creates new HallInMemoryDB object
     */
    private HallInMemoryDB() {
    }

    /**
     * Returns sole object of this class
     * @return HallInMemoryDB object
     */
    public static HallInMemoryDB getInstance() {
        return hallInMemoryDB;
    }

    /**
     * Adds new Hall object to this storage
     * @param hall - object for adding
     */
    @Override
    public void save(Hall hall) {
        synchronized(InMemoryDB.LOCKER) {
            hall.setPerformances(new ArrayList<>());
            super.save(hall);
        }
    }

    /**
     * Replaces Hall object by key with new object
     * @param hall - new object
     * @return 'true' if element with the same key was successfully updated, 'false' otherwise
     */
    @Override
    public boolean update(Hall hall) {
        synchronized(InMemoryDB.LOCKER) {
            Hall oldHall = replaceWith(hall);
            if (oldHall != null) {
                //(Hall with the same key was in DB)

                hall.setPerformances(oldHall.getPerformances());
                for (Performance performance : hall.getPerformances()) {
                    performance.setHall(hall);
                }
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Deletes key-value pair
     * @param key - key
     * @return 'true' if pair with this key was successfully deleted, 'false' otherwise
     */
    @Override
    public boolean delete(Integer key) {
        synchronized(InMemoryDB.LOCKER) {
            Hall hall = getById(key);
            return hall != null && !(hall.getPerformances().size() > 0) && super.delete(key);
        }
    }

}
