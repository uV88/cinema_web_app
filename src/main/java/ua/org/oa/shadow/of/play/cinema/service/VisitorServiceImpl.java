package ua.org.oa.shadow.of.play.cinema.service;

import ua.org.oa.shadow.of.play.cinema.dao.api.VisitorDao;
import ua.org.oa.shadow.of.play.cinema.dto.VisitorDTO;
import ua.org.oa.shadow.of.play.cinema.model.Visitor;
import ua.org.oa.shadow.of.play.cinema.service.api.VisitorService;

/**
 * Created by volodymyr on 21.03.17.
 *
 * Class implements VisitorService interface for keeping objects of class VisitorDTO.
 */
public class VisitorServiceImpl extends ServiceImpl<VisitorDTO, Visitor> implements VisitorService<Integer, VisitorDTO> {

    private VisitorDao<Integer, Visitor> visitorDao;

    protected VisitorServiceImpl(VisitorDao<Integer, Visitor> visitorDao) {
        super(VisitorDTO.class, Visitor.class,
                visitorDao);
        this.visitorDao = visitorDao;
    }

    @Override
    public VisitorDTO getByLogin(String login) {
        Visitor visitor = visitorDao.getByLogin(login);
        return (visitor != null) ? ServiceImpl.BEAN_MAPPER.singleMapper(visitor, VisitorDTO.class) : null;
    }

}
