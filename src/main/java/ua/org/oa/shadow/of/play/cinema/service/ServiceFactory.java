package ua.org.oa.shadow.of.play.cinema.service;

import ua.org.oa.shadow.of.play.cinema.dao.DaoFactory;
import ua.org.oa.shadow.of.play.cinema.dto.*;
import ua.org.oa.shadow.of.play.cinema.model.*;
import ua.org.oa.shadow.of.play.cinema.service.api.Service;
import ua.org.oa.shadow.of.play.cinema.service.api.VisitorService;

/**
 * Created by volodymyr on 10.03.17.
 *
 * Static methods of this class returns implementations of Service interface for each type of data transfer object.
 */
public final class ServiceFactory {

    private static Service<Integer, SimpleMovieDTO> simpleMovieDTOService;
    private static Service<Integer, MovieDTO> movieDTOService;
    private static Service<Integer, SimpleHallDTO> simpleHallDTOService;
    private static Service<Integer, SimplePerformanceDTO> simplePerformanceDTOService;
    private static Service<Integer, PerformanceDTO> performanceDTOService;
    private static VisitorService<Integer, SimpleVisitorDTO> simpleVisitorDTOService;
    private static VisitorService<Integer, VisitorDTO> visitorDTOService;
    private static Service<Integer, TicketDTO> ticketDTOService;

    /**
     * Returns sole object as variable of type Service<Integer, SimpleMovieDTO>
     * @return variable of type Service<Integer, SimpleMovieDTO>
     */
    public static synchronized Service<Integer, SimpleMovieDTO> getSimpleMovieDTOService() {
        if(simpleMovieDTOService == null) {
            simpleMovieDTOService = new ServiceImpl<>(SimpleMovieDTO.class, Movie.class, DaoFactory.getMovieDao());
        }
        return simpleMovieDTOService;
    }

    /**
     * Returns sole object as variable of type Service<Integer, MovieDTO>
     * @return variable of type Service<Integer, MovieDTO>
     */
    public static synchronized Service<Integer, MovieDTO> getMovieDTOService() {
        if(movieDTOService == null) {
            movieDTOService = new ServiceImpl<>(MovieDTO.class, Movie.class, DaoFactory.getMovieDao());
        }
        return movieDTOService;
    }

    /**
     * Returns sole object as variable of type Service<Integer, SimpleHallDTO>
     * @return variable of type Service<Integer, SimpleHallDTO>
     */
    public static synchronized Service<Integer, SimpleHallDTO> getSimpleHallDTOService() {
        if(simpleHallDTOService == null) {
            simpleHallDTOService = new ServiceImpl<>(SimpleHallDTO.class, Hall.class, DaoFactory.getHallDao());
        }
        return simpleHallDTOService;
    }

    /**
     * Returns sole object as variable of type Service<Integer, SimplePerformanceDTO>
     * @return variable of type Service<Integer, SimplePerformanceDTO>
     */
    public static synchronized Service<Integer, SimplePerformanceDTO> getSimplePerformanceDTOService() {
        if(simplePerformanceDTOService == null) {
            simplePerformanceDTOService = new ServiceImpl<>(SimplePerformanceDTO.class, Performance.class,
                    DaoFactory.getPerformanceDao());
        }
        return simplePerformanceDTOService;
    }

    /**
     * Returns sole object as variable of type Service<Integer, PerformanceDTO>
     * @return variable of type Service<Integer, PerformanceDTO>
     */
    public static synchronized Service<Integer, PerformanceDTO> getPerformanceDTOService() {
        if(performanceDTOService == null) {
            performanceDTOService = new ServiceImpl<>(PerformanceDTO.class, Performance.class,
                    DaoFactory.getPerformanceDao());
        }
        return performanceDTOService;
    }

    /**
     * Returns sole object as variable of type VisitorService<Integer, SimpleVisitorDTO>
     * @return variable of type VisitorService<Integer, SimpleVisitorDTO>
     */
    public static synchronized VisitorService<Integer, SimpleVisitorDTO> getSimpleVisitorDTOService() {
        if(simpleVisitorDTOService == null) {
            simpleVisitorDTOService = new SimpleVisitorServiceImpl(DaoFactory.getVisitorDao());
        }
        return simpleVisitorDTOService;
    }

    /**
     * Returns sole object as variable of type VisitorService<Integer, VisitorDTO>
     * @return variable of type VisitorService<Integer, VisitorDTO>
     */
    public static synchronized VisitorService<Integer, VisitorDTO> getVisitorDTOService() {
        if(visitorDTOService == null) {
            visitorDTOService = new VisitorServiceImpl(DaoFactory.getVisitorDao());
        }
        return visitorDTOService;
    }

    /**
     * Returns sole object as variable of type Service<Integer, TicketDTO>
     * @return variable of type Service<Integer, TicketDTO>
     */
    public static synchronized Service<Integer, TicketDTO> getTicketDTOService() {
        if(ticketDTOService == null) {
            ticketDTOService = new ServiceImpl<>(TicketDTO.class, Ticket.class,
                    DaoFactory.getTicketDao());
        }
        return ticketDTOService;
    }

}
