package ua.org.oa.shadow.of.play.cinema.filters;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 22.03.17.
 *
 * Filter checks on visitor authorization as admin.
 */
@WebFilter(filterName = "AdminFilter", urlPatterns = {
        "/adminregistration",
        "/visitor", "/blockvisitor", "/unblockvisitor",
        "/hall",
        "/delhall", "/purposehall", "/implhall",
        "/delmovie", "/purposemovie", "/implmovie",
        "/addposter",
        "/delperfor", "/purposeperfor", "/implperfor",
        "/repayment"})
public class AdminFilter implements Filter {
    public void destroy() {
        // nothing to do
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        SimpleVisitorDTO visitor = (SimpleVisitorDTO) request.getSession().getAttribute("visitor");
        if(visitor != null && visitor.getRole() == SimpleVisitorDTO.Role.ADMIN) {
            chain.doFilter(req, resp);
        }
        else {
            HttpServletResponse response = (HttpServletResponse) resp;
            response.sendRedirect(request.getContextPath() + "/auth");
        }
    }

    public void init(FilterConfig config) throws ServletException {
        // nothing to do
    }

}
