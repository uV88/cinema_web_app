package ua.org.oa.shadow.of.play.cinema.checkers;

import java.util.regex.Pattern;

/**
 * Created by volodymyr on 02.04.17.
 *
 * Single static method of this class provides control of correctness of email address
 */
public final class EmailChecker {

    private static final Pattern EMAIL_PATTERN = Pattern.compile("[a-zA-Z\\d.]+@[a-z]+\\.[a-z]{1,3}");

    /**
     * Returns 'true' if string is a email address
     * @param emailAddress - string
     * @return 'true' if string is a email address, 'false' otherwise
     */
    public static boolean isEmailAddress(String emailAddress) {
        return EMAIL_PATTERN.matcher(emailAddress).matches();
    }

}
