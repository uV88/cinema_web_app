package ua.org.oa.shadow.of.play.cinema.dao.inmemorydb;

import ua.org.oa.shadow.of.play.cinema.dao.api.VisitorDao;
import ua.org.oa.shadow.of.play.cinema.model.Ticket;
import ua.org.oa.shadow.of.play.cinema.model.Visitor;

import java.util.ArrayList;

/**
 * Created by volodymyr on 27.03.17.
 *
 * Class implements VisitorDao interface for keeping objects of class Visitor in RAM data storage.
 * Overridden methods of this class make behaviour of memory DB similar to behaviour of relative DB.
 */
public final class VisitorInMemoryDB extends InMemoryDB<Visitor> implements VisitorDao<Integer, Visitor> {

    private static final VisitorInMemoryDB visitorInMemoryDB = new VisitorInMemoryDB();

    /**
     * Creates new VisitorInMemoryDB object
     */
    private VisitorInMemoryDB() {
    }

    /**
     * Returns sole object of this class
     * @return VisitorInMemoryDB object
     */
    public static VisitorInMemoryDB getInstance() {
        return visitorInMemoryDB;
    }

    /**
     * Returns Visitor object by "login"
     * @param login - "login"
     * @return Visitor object according to it's "login"
     */
    @Override
    public Visitor getByLogin(String login) {
        synchronized(InMemoryDB.LOCKER) {
            for (Visitor visitor : this) {
                if (visitor.getLogin().equals(login)) {
                    return visitor;
                }
            }
            return null;
        }
    }

    /**
     * Adds new Visitor object to this storage
     * @param visitor - object for adding
     */
    @Override
    public void save(Visitor visitor) {
        synchronized(InMemoryDB.LOCKER) {
            visitor.setTickets(new ArrayList<>());
            super.save(visitor);
        }
    }

    /**
     * Replaces Visitor object by key with new object
     * @param visitor - new object
     * @return 'true' if element with the same key was successfully updated, 'false' otherwise
     */
    @Override
    public boolean update(Visitor visitor) {
        synchronized(InMemoryDB.LOCKER) {
            Visitor oldVisitor = replaceWith(visitor);
            if (oldVisitor != null) {
                //(Visitor with the same key was in DB)

                visitor.setTickets(oldVisitor.getTickets());
                for (Ticket ticket : visitor.getTickets()) {
                    ticket.setVisitor(visitor);
                }
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Deletes key-value pair
     * @param key - key
     * @return 'true' if pair with this key was successfully deleted, 'false' otherwise
     */
    @Override
    public boolean delete(Integer key) {
        synchronized(InMemoryDB.LOCKER) {
            Visitor visitor = getById(key);
            return visitor != null && !(visitor.getTickets().size() > 0) && super.delete(key);
        }
    }

}
