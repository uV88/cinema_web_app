package ua.org.oa.shadow.of.play.cinema.dao.impl;

import ua.org.oa.shadow.of.play.cinema.model.Performance;
import ua.org.oa.shadow.of.play.cinema.model.Ticket;
import ua.org.oa.shadow.of.play.cinema.model.Visitor;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class implements Dao interface for keeping objects of class Ticket in data storage.
 */
public final class TicketDaoImpl extends CrudDAO<Ticket> {

    private static final String SELECT_FROM =
            "SELECT * FROM Ticket NATURAL JOIN (Performance NATURAL JOIN (Movie, Hall), Visitor)";
    private static final String ORDER_BY ="ORDER BY rowNumber, seatNumber";
    private static final TicketDaoImpl crudDAO = new TicketDaoImpl();

    private final String SELECT_FROM_FOR_PERFORMANCE =
            "SELECT * FROM Ticket NATURAL JOIN Visitor WHERE performance_id=?";
    private final String SELECT_FROM_FOR_VISITOR =
            "SELECT * FROM Ticket NATURAL JOIN (Performance NATURAL JOIN (Movie, Hall)) WHERE visitor_id=?";
    private final String INSERT = "INSERT INTO Ticket (rowNumber, seatNumber, price, performance_id, visitor_id) " +
            "VALUES (?, ?, ?, ?, ?)";
    private final String UPDATE = "UPDATE Ticket SET rowNumber=?, seatNumber=?, price=?, performance_id=?, visitor_id=? " +
            "WHERE ticket_id=?";

    /**
     * Creates new MovieDaoImpl object
     */
    private TicketDaoImpl() {
        super(SELECT_FROM, ORDER_BY, Ticket.class);
    }

    /**
     * Returns sole object of this class
     * @return TicketDaoImpl object
     */
    public static TicketDaoImpl getInstance() {
        return crudDAO;
    }

    /**
     * Returns all Ticket objects, refer to specified Performance object
     * @param performance - specified performance
     * @return list of tickets refer to specified performance
     */
    List<Ticket> selectFor(Performance performance) {
        return selectFor(performance, null);
    }

    /**
     * Returns all Ticket objects, refer to specified Visitor object
     * @param visitor - specified visitor
     * @return list of tickets refer to specified visitor
     */
    List<Ticket> selectFor(Visitor visitor) {
        return selectFor(null, visitor);
    }


    private List<Ticket> selectFor(Performance performance, Visitor visitor) {
        List<Ticket> list = new ArrayList<>();
        String sql;
        Integer id;
        if(performance != null) {
            sql = SELECT_FROM_FOR_PERFORMANCE;
            id = performance.getId();
        }
        else if(visitor != null) {
            sql = SELECT_FROM_FOR_VISITOR;
            id = visitor.getId();
        }
        else {
            return list;
        }
        System.out.println(sql);
        try(Connection connection = CrudDAO.DATA_SOURCE.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            fillList(resultSet, list, performance, visitor);
            resultSet.close();
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Fills list with new Ticket objects created according to the rows of the ResultSet
     * @param resultSet - ResultSet object
     * @param list - list for adding new elements
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected void fillList(ResultSet resultSet, List<Ticket> list) throws SQLException {
        fillList(resultSet, list, null, null);
    }

    /**
     * Fills list with new objects of type Ticket created according to the rows of the ResultSet
     * @param resultSet - ResultSet object
     * @param list - list for adding new elements
     * @param statedPerformance - reference for adding to new elements as value of the field
     * @param statedVisitor - reference for adding to new elements as value of the field
     * @throws SQLException - if a database error occurs
     */
    private void fillList(ResultSet resultSet, List<Ticket> list, Performance statedPerformance, Visitor statedVisitor) throws SQLException {
        System.out.println("Tickets for performance: " + statedPerformance + ", for visitor: " + statedVisitor);

        Map<Integer, Performance> performanceMap = null;
        if(statedPerformance == null) {
            performanceMap = new HashMap<>();
        }

        Map<Integer, Visitor> visitorMap = null;
        if(statedVisitor == null) {
            visitorMap = new HashMap<>();
        }

        while(resultSet.next()) {
            Ticket ticket = prepareTicket(resultSet);

            if(performanceMap != null) {
                int performanceId = resultSet.getInt("performance_id");
                Performance performance = performanceMap.get(performanceId);
                if(performance == null) {
                    performance = PerformanceDaoImpl.getInstance().readOne(resultSet);
                    performanceMap.put(performanceId, performance);
                }
                ticket.setPerformance(performance);
            }
            else {
                ticket.setPerformance(statedPerformance);
            }

            if(visitorMap != null) {
                int visitorId = resultSet.getInt("visitor_id");
                Visitor visitor = visitorMap.get(visitorId);
                if (visitor == null) {
                    visitor = VisitorDaoImpl.getInstance().readOne(resultSet);
                    visitorMap.put(visitorId, visitor);
                }
                ticket.setVisitor(visitor);
            }
            else {
                ticket.setVisitor(statedVisitor);
            }

            list.add(ticket);
        }
    }

    /**
     * Returns new object of type Ticket creating according to the row of the ResultSet
     * @param resultSet - ResultSet object
     * @return object of type Ticket
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected Ticket readOne(ResultSet resultSet) throws SQLException {
        Ticket ticket = prepareTicket(resultSet);
        ticket.setPerformance(PerformanceDaoImpl.getInstance().readOne(resultSet));
        ticket.setVisitor(VisitorDaoImpl.getInstance().readOne(resultSet));
        return ticket;
    }

    private Ticket prepareTicket(ResultSet resultSet) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setId(resultSet.getInt("ticket_id"));
        ticket.setRowNumber(resultSet.getInt("rowNumber"));
        ticket.setSeatNumber(resultSet.getInt("seatNumber"));
        ticket.setPrice(resultSet.getInt("price"));
        return ticket;
    }

    /**
     * Creates new PreparedStatement object for adding new row to the database table
     * @param connection - Connection
     * @param element - object converting to the table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Ticket element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        fill(preparedStatement, element);
        return preparedStatement;
    }

    /**
     * Creates new PreparedStatement object for updating row in the database table
     * @param connection - Connection
     * @param element - object converting to the new values of table's row
     * @return PreparedStatement
     * @throws SQLException - if a database error occurs
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Ticket element) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        fill(preparedStatement, element);
        preparedStatement.setInt(6, element.getId());
        return preparedStatement;
    }

    private void fill(PreparedStatement preparedStatement, Ticket element) throws SQLException {
        preparedStatement.setInt(1, element.getRowNumber());
        preparedStatement.setInt(2, element.getSeatNumber());
        preparedStatement.setInt(3, element.getPrice());
        preparedStatement.setInt(4, element.getPerformance().getId());
        preparedStatement.setInt(5, element.getVisitor().getId());
    }

}
