package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 02.04.17.
 *
 * Servlet edits user's profile: marks user as "blocked" or "unblocked".
 */
@WebServlet(name = "BlockUserServlet", urlPatterns = {"/blockvisitor", "/unblockvisitor"})
public class BlockUserServlet extends HttpServlet {

    /**
     * Marks user as "blocked" or "unblocked".
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer visitorId = Integer.valueOf(request.getParameter("visitorId"));
        SimpleVisitorDTO simpleVisitorDTO = ServiceFactory.getSimpleVisitorDTOService().getById(visitorId);
        String message;
        String returnUrl;
        if(simpleVisitorDTO != null && simpleVisitorDTO.getRole() != SimpleVisitorDTO.Role.ADMIN) {
            String destination = request.getServletPath();
            switch (destination) {
                case "/blockvisitor":
                    simpleVisitorDTO.setBlocked(true);
                    message = "User has been successfully blocked!";
                    break;
                case "/unblockvisitor":
                    simpleVisitorDTO.setBlocked(false);
                    message = "User has been successfully unblocked!";
                    break;
                default: message = "";
            }
            if(! ServiceFactory.getSimpleVisitorDTOService().update(simpleVisitorDTO)) {
                message = "Something went wrong!";
            }
            returnUrl = request.getContextPath() + "/visitor?id=" + visitorId;
        }
        else {
            message = "Visitor doesn't exist or he is admin!";
            returnUrl = request.getContextPath() + "/visitor";
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Ignores request, sends redirect to observe list of "visitors".
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/visitor");
    }

}
