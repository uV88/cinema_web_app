package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.PerformanceDTO;
import ua.org.oa.shadow.of.play.cinema.dto.SimplePerformanceDTO;
import ua.org.oa.shadow.of.play.cinema.dto.TicketDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by volodymyr on 19.03.17.
 *
 * Servlet prepares single "performance" or list of "performances" for observing and buying a ticket.
 */
@WebServlet(name = "PerformanceServlet", urlPatterns = {"/performance"})
public class PerformanceServlet extends HttpServlet {

    /**
     * Calls doGet(request, response).
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Prepares single "performance" or list of "performances" for observing.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String performanceIdString = request.getParameter("id");
        if(performanceIdString != null && performanceIdString.length() != 0) {
            Integer performanceId = Integer.valueOf(performanceIdString);
            PerformanceDTO performanceDTO = ServiceFactory.getPerformanceDTOService().getById(performanceId);
            if(performanceDTO != null) {
                request.setAttribute("performanceDTO", performanceDTO);

                if(performanceDTO.isForSale()) {
                    int[][] disposedPrices = disposePrices(performanceDTO.getSimpleHallDTO().getSeatsScheme(),
                            performanceDTO.getPricesForRows(), performanceDTO.getTicketsDTO());
                    request.setAttribute("disposedPrices", disposedPrices);
                }

                request.getRequestDispatcher("pages/common/performance.jsp").forward(request, response);
            }
            else {
                //performance with this 'id' does not exist
                response.sendRedirect(request.getContextPath() + "/performance");
            }
        }
        else {
            List<SimplePerformanceDTO> simplePerformanceDTOList = ServiceFactory.getSimplePerformanceDTOService().getAll();
            request.setAttribute("simplePerformanceDTOList", simplePerformanceDTOList);
            request.getRequestDispatcher("pages/common/performances.jsp").forward(request, response);
        }
    }

    /**
     * Returns hall's seats disposition and price of each seat.
     * @param seatsScheme - list of each row number of seats
     * @param pricesScheme - list of each row price of seat
     * @param ticketDTOs - list of sold tickets
     * @return two-dimension array represented hall's seats disposition and price of each seat
     */
    private int[][] disposePrices(List<Integer> seatsScheme, List<Integer> pricesScheme,
                                  List<TicketDTO> ticketDTOs) {
        //creates scheme of seats of hall
        int[][] disposedPrices = new int[seatsScheme.size()][];
        for(int i = 0; i < disposedPrices.length; i++) {
            disposedPrices[i] = new int[seatsScheme.get(i)];
        }
        //writes price of each seat in this performance
        for(int i = 0; i < disposedPrices.length; i++) {
            Arrays.fill(disposedPrices[i], pricesScheme.get(i));
        }
        //sets price "-1" for bought seat
        for(TicketDTO ticketDTO : ticketDTOs) {
            disposedPrices[ticketDTO.getRowNumber()][ticketDTO.getSeatNumber()] = -1;
        }
        return disposedPrices;
    }

}
