package ua.org.oa.shadow.of.play.cinema.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by volodymyr on 01.04.17.
 *
 * Filter saves request URL as session attribute.
 */
@WebFilter(filterName = "SaveLastFilter", urlPatterns = {"/movie", "/hall", "/performance", "/visitor", "/me"})
public class SaveLastFilter implements Filter {
    public void destroy() {
        // nothing to do
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        StringBuilder url = new StringBuilder().append(request.getContextPath()).append(request.getServletPath());
        String queryString = request.getQueryString();
        if(queryString != null) {
            url.append("?").append(queryString);
        }
        request.getSession().setAttribute("url", url.toString());
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {
        // nothing to do
    }

}
