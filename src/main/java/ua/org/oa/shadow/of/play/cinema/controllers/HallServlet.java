package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleHallDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by volodymyr on 23.03.17.
 *
 * Servlet prepares single "hall" or list of "halls" for observing.
 */
@WebServlet(name = "HallServlet", urlPatterns = "/hall")
public class HallServlet extends HttpServlet {

    /**
     * Calls doGet(request, response).
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Prepares single "hall" or list of "halls" for observing.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String hallIdStr = request.getParameter("id");
        if(hallIdStr != null && hallIdStr.length() != 0) {
            Integer hallId = Integer.valueOf(hallIdStr);
            SimpleHallDTO simpleHallDTO = ServiceFactory.getSimpleHallDTOService().getById(hallId);
            if(simpleHallDTO != null) {
                request.setAttribute("simpleHallDTO", simpleHallDTO);
                request.getRequestDispatcher("pages/admin/hall.jsp").forward(request, response);
            }
            else {
                //hall with this 'id' does not exist
                response.sendRedirect(request.getContextPath() + "/hall");
            }
        }
        else {
            List<SimpleHallDTO> simpleHallDTOList = ServiceFactory.getSimpleHallDTOService().getAll();
            request.setAttribute("simpleHallDTOList", simpleHallDTOList);
            request.getRequestDispatcher("pages/admin/halls.jsp").forward(request, response);
        }

    }
}
