package ua.org.oa.shadow.of.play.cinema.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by volodymyr on 23.03.17.
 *
 * Filter sets character encoding of request.
 */
@WebFilter(filterName = "EncodingFilter", urlPatterns = {"/*"})
public class EncodingFilter implements Filter {

    private static final String CHARACTER_ENCODING = "UTF-8";

    public void destroy() {
        // nothing to do
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        req.setCharacterEncoding(CHARACTER_ENCODING);
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {
        // nothing to do
    }

}
