package ua.org.oa.shadow.of.play.cinema.dto;

import ua.org.oa.shadow.of.play.cinema.model.Entity;

/**
 * Created by volodymyr on 07.02.17.
 *
 * Class represents cinema ticket
 */
public class TicketDTO extends Entity {

    private SimplePerformanceDTO simplePerformanceDTO;
    private int rowNumber;
    private int seatNumber;
    private int price;
    private SimpleVisitorDTO simpleVisitorDTO;

    /**
     * Creates new TicketDTO object
     */
    public TicketDTO() {
    }

    /**
     * Creates new TicketDTO object
     * @param simplePerformanceDTO - simplePerformanceDTO, this ticket for
     * @param rowNumber - number of the row in the hall
     * @param seatNumber - number of the seat in the row
     * @param price - ticket's price
     * @param simpleVisitorDTO - ticket's owner
     */
    public TicketDTO(SimplePerformanceDTO simplePerformanceDTO,
                     int rowNumber, int seatNumber, int price, SimpleVisitorDTO simpleVisitorDTO) {
        this.simplePerformanceDTO = simplePerformanceDTO;
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.price = price;
        this.simpleVisitorDTO = simpleVisitorDTO;
    }

    public SimplePerformanceDTO getSimplePerformanceDTO() {
        return simplePerformanceDTO;
    }

    public void setSimplePerformanceDTO(SimplePerformanceDTO simplePerformanceDTO) {
        this.simplePerformanceDTO = simplePerformanceDTO;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public SimpleVisitorDTO getSimpleVisitorDTO() {
        return simpleVisitorDTO;
    }

    public void setSimpleVisitorDTO(SimpleVisitorDTO simpleVisitorDTO) {
        this.simpleVisitorDTO = simpleVisitorDTO;
    }

    @Override
    public String toString() {
        return "TicketDTO{" +
                "id=" + getId() +
                ", simplePerformanceDTO=" + simplePerformanceDTO +
                ", visitorDTO=" + (simpleVisitorDTO == null ? "null" : simpleVisitorDTO.getLogin()) +
                ", rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", price=" + price +
                '}';
    }
}
