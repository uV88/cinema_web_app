package ua.org.oa.shadow.of.play.cinema.dto;

import ua.org.oa.shadow.of.play.cinema.model.Entity;

/**
 * Created by volodymyr on 01.04.17.
 *
 * Class represents visitor profile (without list of tickets).
 */
public class SimpleVisitorDTO extends Entity {

    private String login;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private Role role;
    private boolean blocked;

    /**
     * Creates new SimpleVisitorDTO object
     */
    public SimpleVisitorDTO() {
    }

    /**
     * Creates new SimpleVisitorDTO object
     * @param login - visitor's login
     * @param firstName - visitor's first name
     * @param emailAddress - visitor's email address
     * @param password - visitor's password
     * @param role - visitor's role
     */
    public SimpleVisitorDTO(String login, String firstName, String emailAddress, String password, Role role) {
        this.login = login;
        this.firstName = firstName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    @Override
    public String toString() {
        return "SimpleVisitorDTO{" +
                "id=" + getId() +
                ", login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public enum Role{USER, ADMIN}

}
