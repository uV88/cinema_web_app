package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;
import ua.org.oa.shadow.of.play.cinema.checkers.HtmlReplacer;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 21.03.17.
 *
 * Servlet provides authorization by login-password.
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/auth", "/out"})
public class LoginServlet extends HttpServlet {

    /**
     * Makes authorization by login-password.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        SimpleVisitorDTO simpleVisitorDTO =
                ServiceFactory.getSimpleVisitorDTOService().getByLogin(HtmlReplacer.replaceChars(login));
        if(simpleVisitorDTO != null && password.equals(simpleVisitorDTO.getPassword())) {
            if(!simpleVisitorDTO.isBlocked()) {
                request.getSession().setAttribute("visitor", simpleVisitorDTO);
                String url = (String) request.getSession().getAttribute("url");
                response.sendRedirect(url);
            }
            else {
                String message = "Your account \"" + simpleVisitorDTO.getLogin() + "\" is blocked.";
                request.setAttribute("message", message);
                request.getRequestDispatcher("pages/common/auth.jsp").forward(request, response);
            }
        }
        else {
            String message = "Illegal login or/and password. Try again.";
            request.setAttribute("message", message);
            request.getRequestDispatcher("pages/common/auth.jsp").forward(request, response);
        }
    }

    /**
     * Log out or prepare form for authorization.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destination = request.getServletPath();
        switch (destination) {
            case "/out" : request.getSession().removeAttribute("visitor");
                String url = (String) request.getSession().getAttribute("url");
                response.sendRedirect(url);
                break;
            case "/auth" : String message = "Bring login and password";
                request.setAttribute("message", message);
                request.getRequestDispatcher("pages/common/auth.jsp").forward(request, response);
        }

    }
}
