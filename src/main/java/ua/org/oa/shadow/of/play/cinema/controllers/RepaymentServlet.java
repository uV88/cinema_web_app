package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 28.03.17.
 *
 * Servlet deletes "tickets".
 */
@WebServlet(name = "RepaymentServlet", urlPatterns = {"/repayment"})
public class RepaymentServlet extends HttpServlet {

    /**
     * Deletes "tickets" from DB.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] ticketsId = request.getParameterValues("ticketId");
        String message;
        if(ticketsId != null) {
            boolean successfully = true;
            for (String ticketId : ticketsId) {
                Integer id = Integer.valueOf(ticketId);
                if (!ServiceFactory.getTicketDTOService().delete(id)) {
                    successfully = false;
                }
            }
            if (successfully) {
                message = "All tickets are given back!";
            } else {
                message = "Some tickets are not given back!";
            }
        }
        else {
            message = "Choose tickets for repayment";
        }
        StringBuilder returnUrl = new StringBuilder(request.getContextPath());
        String performanceIdString = request.getParameter("performanceId");
        String visitorIdString = request.getParameter("visitorId");
        if(performanceIdString != null) {
            returnUrl =  returnUrl.append("/performance?id=").append(performanceIdString);
        }
        else if(visitorIdString != null) {
            returnUrl = returnUrl.append("/visitor?id=").append(visitorIdString);
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl.toString());
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Ignores request, sends redirect to observe list of "performances".
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/performance");
    }
}
