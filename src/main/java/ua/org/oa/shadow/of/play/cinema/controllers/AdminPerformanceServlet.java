package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleHallDTO;
import ua.org.oa.shadow.of.play.cinema.dto.SimpleMovieDTO;
import ua.org.oa.shadow.of.play.cinema.dto.SimplePerformanceDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by volodymyr on 25.03.17.
 *
 * Servlet deletes, updates and adds "performance" object.
 */
@WebServlet(name = "AdminPerformanceServlet", urlPatterns = {"/delperfor", "/purposeperfor", "/implperfor"})
public class AdminPerformanceServlet extends HttpServlet {

    /**
     * Method makes operation according to servlet path of request.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destination = request.getServletPath();
        switch (destination) {
            case "/delperfor":
                deletePerformance(request, response);
                break;
            case "/purposeperfor":
                preparePerformance(request, response);
                break;
            case "/implperfor":
                implementPerformance(request, response);
        }
    }

    /**
     * Deletes prepared for changing "performance" object from session.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String identification = request.getParameter("identification");
        request.getSession().removeAttribute("changedSimplePerformanceDTO" + identification);
        request.setAttribute("message", "All your choices are canceled");
        request.setAttribute("returnUrl", request.getContextPath() + "/performance");
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Deletes "performance" by it's key.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void deletePerformance(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer performanceId = Integer.valueOf(request.getParameter("performanceId"));
        String message;
        String returnUrl;
        if(ServiceFactory.getPerformanceDTOService().delete(performanceId)) {
            message = "Performance has been successfully deleted!";
            returnUrl = request.getContextPath() + "/performance";
        }
        else {
            message = "Something went wrong!";
            returnUrl = request.getContextPath() + "/performance?id=" + performanceId;
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Prepares "performance" for updating or creates template for new "performance", saves it in session.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void preparePerformance(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SimplePerformanceDTO simplePerformanceDTO;
        String performanceId = request.getParameter("performanceId");
        if(performanceId != null) {
            /*
             * admin chose performance to edit
             */
            Integer id = Integer.valueOf(performanceId);
            simplePerformanceDTO = ServiceFactory.getSimplePerformanceDTOService().getById(id);
            //to prevent NullPointerException:
            if(simplePerformanceDTO == null) {
                request.setAttribute("message", "Performance doesn't exists!");
                request.setAttribute("returnUrl", request.getContextPath() + "/performance");
                request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
                return;
            }
        }
        else {
            /*
             * admin wants to add new performance
             *
             * It's possible to choose movie and hall
             * only at the moment of performance creating
             */
            String movieIdString = request.getParameter("movieId");
            if(movieIdString != null) {
                request.getSession().setAttribute("movieIdForPerformance", Integer.valueOf(movieIdString));
            }
            String hallIdString = request.getParameter("hallId");
            if(hallIdString != null) {
                request.getSession().setAttribute("hallIdForPerformance", Integer.valueOf(hallIdString));
            }
            if(request.getSession().getAttribute("movieIdForPerformance") == null) {
                String message = "You must choose movie for performance " +
                        "by pressing \"ADD PERFORMANCE\" at selected movie";
                String returnUrl = request.getContextPath() + "/movie";
                request.setAttribute("message", message);
                request.setAttribute("returnUrl", returnUrl);
                request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
                return;
            }
            if(request.getSession().getAttribute("hallIdForPerformance") == null) {
                String message = "You must choose hall for performance " +
                        "by pressing \"ADD PERFORMANCE\" at selected hall";
                String returnUrl = request.getContextPath() + "/hall";
                request.setAttribute("message", message);
                request.setAttribute("returnUrl", returnUrl);
                request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
                return;
            }

            simplePerformanceDTO = new SimplePerformanceDTO();
            simplePerformanceDTO.setId(-1);

            Integer movieId = (Integer) request.getSession().getAttribute("movieIdForPerformance");
            request.getSession().removeAttribute("movieIdForPerformance");
            SimpleMovieDTO simpleMovieDTO = ServiceFactory.getSimpleMovieDTOService().getById(movieId);
            Integer hallId = (Integer) request.getSession().getAttribute("hallIdForPerformance");
            request.getSession().removeAttribute("hallIdForPerformance");
            SimpleHallDTO simpleHallDTO = ServiceFactory.getSimpleHallDTOService().getById(hallId);
            //to prevent NullPointerException:
            if(simpleMovieDTO == null || simpleHallDTO == null) {
                request.setAttribute("message", "Movie or hall doesn't exists!");
                request.setAttribute("returnUrl", request.getContextPath() + "/performance");
                request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
                return;
            }

            simplePerformanceDTO.setSimpleMovieDTO(simpleMovieDTO);
            simplePerformanceDTO.setSimpleHallDTO(simpleHallDTO);

            List<Integer> pricesForRows = new ArrayList<>(simpleHallDTO.getSeatsScheme().size());
            for(int i = 0; i < simpleHallDTO.getSeatsScheme().size(); i++) {
                pricesForRows.add(0);
            }

            simplePerformanceDTO.setPricesForRows(pricesForRows);
        }
        request.setAttribute("changedSimplePerformanceDTO", simplePerformanceDTO);
        String identification = String.valueOf(System.currentTimeMillis());
        request.setAttribute("identification", identification);
        request.getSession().setAttribute("changedSimplePerformanceDTO" + identification, simplePerformanceDTO);
        request.getRequestDispatcher("pages/admin/chPerformance.jsp").forward(request, response);
    }

    /**
     * Adds changes to saved in session "performance", updates "performance" in DB or adds new one.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void implementPerformance(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String identification = request.getParameter("identification");
        SimplePerformanceDTO simplePerformanceDTO = (SimplePerformanceDTO)
                request.getSession().getAttribute("changedSimplePerformanceDTO" + identification);
        if(simplePerformanceDTO == null) {
            request.setAttribute("message", "Your request is ignored!");
            request.setAttribute("returnUrl", request.getContextPath() + "/performance");
            request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
            return;
        }
        request.getSession().removeAttribute("changedSimplePerformanceDTO" + identification);

        LocalDate date = LocalDate.parse(request.getParameter("date"), DateTimeFormatter.ISO_LOCAL_DATE);
        simplePerformanceDTO.setDate(date);
        LocalTime time = LocalTime.parse(request.getParameter("time"), DateTimeFormatter.ISO_LOCAL_TIME);
        simplePerformanceDTO.setTime(time);
        simplePerformanceDTO.setForSale(request.getParameter("forSale") != null);

        String message;
        String returnUrl;
        if(simplePerformanceDTO.getId() < 0) {
            /*
             * it's possible to set prices for rows
             * only at the moment of saving new performance
             */
            SimpleHallDTO simpleHallDTO = simplePerformanceDTO.getSimpleHallDTO();
            List<Integer> pricesForRows = new ArrayList<>(simpleHallDTO.getSeatsScheme().size());
            for(int i = 0; i < simpleHallDTO.getSeatsScheme().size(); i++) {
                pricesForRows.add(Integer.valueOf(request.getParameter("price" + i)));
            }
            simplePerformanceDTO.setPricesForRows(pricesForRows);
            /*
             * saving
             */
            ServiceFactory.getSimplePerformanceDTOService().save(simplePerformanceDTO);
            if(simplePerformanceDTO.getId() > 0) {
                message = "Performance has been successfully added!";
                returnUrl = request.getContextPath() + "/performance?id=" + simplePerformanceDTO.getId();
            }
            else {
                message = "Something went wrong!";
                returnUrl = request.getContextPath() + "/performance";
            }
        }
        else {
            /*
             * updating
             */
            if(ServiceFactory.getSimplePerformanceDTOService().update(simplePerformanceDTO)) {
                message = "Performance has been successfully updated!";
            }
            else {
                message = "Something went wrong!";
            }
            returnUrl = request.getContextPath() + "/performance?id=" + simplePerformanceDTO.getId();
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }
}
