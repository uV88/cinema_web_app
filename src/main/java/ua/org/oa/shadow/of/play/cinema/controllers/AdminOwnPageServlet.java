package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.checkers.EmailChecker;
import ua.org.oa.shadow.of.play.cinema.checkers.HtmlReplacer;
import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 02.04.17.
 *
 * Servlet adds new user's profile or updates authorized visitor own profile.
 */
@WebServlet(name = "AdminOwnPageServlet", urlPatterns = {"/editme", "/addme", "/implme"})
public class AdminOwnPageServlet extends HttpServlet {

    /**
     * Adds new USER profile to DB or updates authorized visitor (USER or ADMIN) own profile in DB.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message;
        String returnUrl;
        SimpleVisitorDTO simpleVisitorDTO;
        Integer id = Integer.valueOf(request.getParameter("id"));
        if(id > 0) {
            //visitor edits it's own profile
            returnUrl = request.getContextPath() + "/editme";
            //gets own id from simpleVisitorDTO object of own session:
            int myId = ((SimpleVisitorDTO) request.getSession().getAttribute("visitor")).getId();
            //gets newest version of simpleVisitorDTO object from DB:
            simpleVisitorDTO = ServiceFactory.getSimpleVisitorDTOService().getById(myId);
            String oldPassword = request.getParameter("oldPassword");
            if(!simpleVisitorDTO.getPassword().equals(oldPassword)) {
                message = "Incorrect old password!";
                request.setAttribute("message", message);
                request.setAttribute("returnUrl", returnUrl);
                request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
                return;
            }
        }
        else {
            //visitor registers new profile
            returnUrl = request.getContextPath() + "/addme";
            simpleVisitorDTO = new SimpleVisitorDTO();
            simpleVisitorDTO.setId(-1);
            simpleVisitorDTO.setLogin(HtmlReplacer.replaceChars(request.getParameter("login")));
            simpleVisitorDTO.setRole(SimpleVisitorDTO.Role.USER);
            simpleVisitorDTO.setBlocked(false);
        }
        String password = request.getParameter("password");
        if(request.getParameter("confPassword").equals(password)) {
            String emailAddress = request.getParameter("emailAddress");
            if(EmailChecker.isEmailAddress(emailAddress)) {
                simpleVisitorDTO.setPassword(password);
                simpleVisitorDTO.setEmailAddress(HtmlReplacer.replaceChars(emailAddress));
                simpleVisitorDTO.setFirstName(HtmlReplacer.replaceChars(request.getParameter("firstName")));
                simpleVisitorDTO.setLastName(HtmlReplacer.replaceChars(request.getParameter("lastName")));

                if(simpleVisitorDTO.getId() > 0) {
                    //visitor edits it's own profile
                    if(ServiceFactory.getSimpleVisitorDTOService().update(simpleVisitorDTO)) {
                        //sets newest version of simpleVisitorDTO object into session:
                        request.getSession().setAttribute("visitor", simpleVisitorDTO);
                        message = "Your profile is successfully updated!";
                        returnUrl = request.getContextPath() + "/me";
                    }
                    else {
                        message = "Something went wrong";
                    }
                }
                else {
                    //visitor registers new profile
                    ServiceFactory.getSimpleVisitorDTOService().save(simpleVisitorDTO);
                    if(simpleVisitorDTO.getId() > 0) {
                        //sets new simpleVisitorDTO object into session (makes authorization):
                        request.getSession().setAttribute("visitor", simpleVisitorDTO);
                        message = "You have successfully registered!";
                        returnUrl = request.getContextPath() + "/me";
                    }
                    else {
                        message = "Something went wrong";
                    }
                }
            }
            else {
                message = "Incorrect mail address!";
            }
        }
        else {
            message = "Values of \"Password\" and \"Confirm password\" fields are not the same!";
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Prepares own profile for updating or creates template for new user's profile.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SimpleVisitorDTO simpleVisitorDTO;
        String destination = request.getServletPath();
        if("/editme".equals(destination)) {
            //will edit own profile
            simpleVisitorDTO = (SimpleVisitorDTO) request.getSession().getAttribute("visitor");
        }
        else {
            //will create new profile
            simpleVisitorDTO = new SimpleVisitorDTO();
            simpleVisitorDTO.setId(-1);
        }
        request.setAttribute("simpleVisitorDTO", simpleVisitorDTO);
        request.getRequestDispatcher("pages/common/chOwn.jsp").forward(request, response);
    }
}
