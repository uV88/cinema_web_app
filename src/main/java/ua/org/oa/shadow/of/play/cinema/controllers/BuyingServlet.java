package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimplePerformanceDTO;
import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;
import ua.org.oa.shadow.of.play.cinema.dto.TicketDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 20.03.17.
 *
 * Servlet adds "ticket".
 */
@WebServlet(name = "BuyingServlet", urlPatterns = {"/buy"})
public class BuyingServlet extends HttpServlet {

    /**
     * Adds "ticket" to DB.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String row = request.getParameter("row");
        String seat = request.getParameter("seat");
        String performanceIdString = request.getParameter("performanceId");
        if(row != null && seat != null && performanceIdString != null) {
            int rowNumber = Integer.parseInt(row);
            int seatNumber = Integer.parseInt(seat);
            Integer performanceId = Integer.valueOf(performanceIdString);

            SimplePerformanceDTO simplePerformanceDTO =
                    ServiceFactory.getSimplePerformanceDTOService().getById(performanceId);

            if(simplePerformanceDTO != null && simplePerformanceDTO.isForSale() &&
                    simplePerformanceDTO.getSimpleHallDTO().getSeatsScheme().get(rowNumber) > seatNumber) {

                TicketDTO ticketDTO = new TicketDTO();
                ticketDTO.setRowNumber(rowNumber);
                ticketDTO.setSeatNumber(seatNumber);
                ticketDTO.setSimplePerformanceDTO(simplePerformanceDTO);
                ticketDTO.setPrice(simplePerformanceDTO.getPricesForRows().get(rowNumber));
                SimpleVisitorDTO simpleVisitorDTO = (SimpleVisitorDTO)request.getSession().getAttribute("visitor");
                ticketDTO.setSimpleVisitorDTO(simpleVisitorDTO);

                ServiceFactory.getTicketDTOService().save(ticketDTO);
                request.setAttribute("ticketDTO", ticketDTO);
                request.getRequestDispatcher("pages/common/ticket.jsp").forward(request, response);

            }
            else {
                //non-existent or closed performance or non-existent seat
                response.sendRedirect(request.getContextPath() + "/performance");
            }
        }
        else {
            //just in case
            response.sendRedirect(request.getContextPath() + "/performance");
        }
    }

    /**
     * Ignores request, sends redirect to observe list of "performances".
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/performance");
    }
}
