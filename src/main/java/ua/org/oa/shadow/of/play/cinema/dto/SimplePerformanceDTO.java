package ua.org.oa.shadow.of.play.cinema.dto;

import ua.org.oa.shadow.of.play.cinema.model.Entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by volodymyr on 16.03.17.
 *
 * Class represents cinema performance (without list of tickets).
 */
public class SimplePerformanceDTO extends Entity {

    private LocalDate date;
    private LocalTime time;
    private SimpleHallDTO simpleHallDTO;
    private SimpleMovieDTO simpleMovieDTO;
    private List<Integer> pricesForRows;
    private boolean forSale;

    /**
     * Creates new SimplePerformanceDTO object
     */
    public SimplePerformanceDTO() {
    }

    /**
     * Creates new SimplePerformanceDTO object
     * @param date - date of the performance
     * @param time - time of the performance
     * @param simpleHallDTO - performance's simpleHallDTO
     * @param simpleMovieDTO - performance's simpleMovieDTO
     */
    public SimplePerformanceDTO(LocalDate date, LocalTime time, SimpleHallDTO simpleHallDTO, SimpleMovieDTO simpleMovieDTO) {
        this.date = date;
        this.time = time;
        this.simpleHallDTO = simpleHallDTO;
        this.simpleMovieDTO = simpleMovieDTO;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public SimpleHallDTO getSimpleHallDTO() {
        return simpleHallDTO;
    }

    public void setSimpleHallDTO(SimpleHallDTO simpleHallDTO) {
        this.simpleHallDTO = simpleHallDTO;
    }

    public SimpleMovieDTO getSimpleMovieDTO() {
        return simpleMovieDTO;
    }

    public void setSimpleMovieDTO(SimpleMovieDTO simpleMovieDTO) {
        this.simpleMovieDTO = simpleMovieDTO;
    }

    public List<Integer> getPricesForRows() {
        return pricesForRows;
    }

    public void setPricesForRows(List<Integer> pricesForRows) {
        this.pricesForRows = pricesForRows;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    @Override
    public String toString() {
        return "SimplePerformanceDTO{" +
                "id=" + getId() +
                ", date=" + date +
                ", time=" + time +
                ", simpleHallDTO=" + (simpleHallDTO == null ? "null" : simpleHallDTO.getHallName()) +
                ", movieDTO=" + (simpleMovieDTO == null ? "null" : simpleMovieDTO.getMovieTitle()) +
                '}';
    }
}
