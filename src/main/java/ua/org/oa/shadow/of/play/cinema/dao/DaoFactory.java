package ua.org.oa.shadow.of.play.cinema.dao;

import ua.org.oa.shadow.of.play.cinema.dao.api.Dao;
import ua.org.oa.shadow.of.play.cinema.dao.api.VisitorDao;
import ua.org.oa.shadow.of.play.cinema.dao.impl.*;
import ua.org.oa.shadow.of.play.cinema.dao.inmemorydb.*;
import ua.org.oa.shadow.of.play.cinema.helper.PropertyHolder;
import ua.org.oa.shadow.of.play.cinema.model.*;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Static methods of this class returns Data Access Objects for each type of java bean objects
 */
public final class DaoFactory {

    private static final Dao<Integer, Movie> movieDao;
    private static final Dao<Integer, Hall> hallDao;
    private static final Dao<Integer, Performance> performanceDao;
    private static final Dao<Integer, Ticket> ticketDao;
    private static final VisitorDao<Integer, Visitor> visitorDao;

    static {
        if(PropertyHolder.getInstance().isInMemoryDB()) {
            //use data storage in RAM
            movieDao = MovieInMemoryDB.getInstance();
            hallDao = HallInMemoryDB.getInstance();
            performanceDao = PerformanceInMemoryDB.getInstance();
            ticketDao = TicketInMemoryDB.getInstance();
            visitorDao = VisitorInMemoryDB.getInstance();

            //add first admin profile to data storage:
            Visitor testAdmin = new Visitor();
            testAdmin.setLogin(PropertyHolder.getInstance().getInMemoryDBAdminLogin());
            testAdmin.setPassword(PropertyHolder.getInstance().getInMemoryDBAdminPassword());
            testAdmin.setEmailAddress("admin@mail.test");
            testAdmin.setFirstName("Admin");
            testAdmin.setRole(Visitor.Role.ADMIN);
            testAdmin.setBlocked(false);
            visitorDao.save(testAdmin);

            //add first user profile to data storage:
            Visitor testUser = new Visitor();
            testUser.setLogin(PropertyHolder.getInstance().getInMemoryDBUserLogin());
            testUser.setPassword(PropertyHolder.getInstance().getInMemoryDBUserPassword());
            testUser.setEmailAddress("user@mail.test");
            testUser.setFirstName("User");
            testUser.setRole(Visitor.Role.USER);
            testUser.setBlocked(false);
            visitorDao.save(testUser);
        }
        else{
            //use relative DB
            movieDao = MovieDaoImpl.getInstance();
            hallDao = HallDaoImpl.getInstance();
            performanceDao = PerformanceDaoImpl.getInstance();
            ticketDao = TicketDaoImpl.getInstance();
            visitorDao = VisitorDaoImpl.getInstance();
        }
    }

    /**
     * Private constructor
     */
    private DaoFactory() {
    }

    public static Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }

    public static Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }

    public static Dao<Integer, Performance> getPerformanceDao() {
        return performanceDao;
    }

    public static Dao<Integer, Ticket> getTicketDao() {
        return ticketDao;
    }

    public static VisitorDao<Integer, Visitor> getVisitorDao() {
        return visitorDao;
    }
}
