package ua.org.oa.shadow.of.play.cinema.service.api;

/**
 * Created by volodymyr on 21.03.17.
 *
 * This interface broaden base data storage functionality with possibility to find element by it's "login".
 * It's necessary, if data storage contains user's profiles.
 */
public interface VisitorService<K, V> extends Service<K, V> {

    /**
     * Returns element by "login"
     * @param login - "login"
     * @return element according to it's "login"
     */
    V getByLogin(String login);

}
