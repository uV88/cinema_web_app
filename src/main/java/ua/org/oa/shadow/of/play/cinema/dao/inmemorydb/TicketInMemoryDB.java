package ua.org.oa.shadow.of.play.cinema.dao.inmemorydb;

import ua.org.oa.shadow.of.play.cinema.model.Performance;
import ua.org.oa.shadow.of.play.cinema.model.Ticket;
import ua.org.oa.shadow.of.play.cinema.model.Visitor;

/**
 * Created by volodymyr on 04.04.17.
 *
 * Class implements Dao interface for keeping objects of class Ticket in RAM data storage.
 * Overridden methods of this class make behaviour of memory DB similar to behaviour of relative DB.
 */
public final class TicketInMemoryDB extends InMemoryDB<Ticket> {

    private static final TicketInMemoryDB ticketInMemoryDB = new TicketInMemoryDB();

    /**
     * Creates new TicketInMemoryDB object
     */
    private TicketInMemoryDB() {
    }

    /**
     * Returns sole object of this class
     * @return TicketInMemoryDB object
     */
    public static TicketInMemoryDB getInstance() {
        return ticketInMemoryDB;
    }

    /**
     * Adds new Ticket to this storage
     * @param ticket - object for adding
     */
    @Override
    public void save(Ticket ticket) {
        synchronized(InMemoryDB.LOCKER) {
            Performance performance = PerformanceInMemoryDB.getInstance().getById(ticket.getPerformance().getId());
            Visitor visitor = VisitorInMemoryDB.getInstance().getById(ticket.getVisitor().getId());
            if (performance != null && visitor != null) {
                //(Performance and Visitor are already in DB)
                ticket.setPerformance(performance);
                ticket.setVisitor(visitor);
                super.save(ticket);
                //add this Ticket to the tickets list of Performance:
                ticket.getPerformance().getTickets().add(ticket);
                //add this Ticket to the tickets list of Visitor:
                ticket.getVisitor().getTickets().add(ticket);
            }
        }
    }

    /**
     * Replaces Ticket object by key with new object
     * @param ticket - new object
     * @return 'true' if element with the same key was successfully updated, 'false' otherwise
     */
    @Override
    public boolean update(Ticket ticket) {
        synchronized(InMemoryDB.LOCKER) {
            Performance performance = PerformanceInMemoryDB.getInstance().getById(ticket.getPerformance().getId());
            Visitor visitor = VisitorInMemoryDB.getInstance().getById(ticket.getVisitor().getId());
            if (performance != null && visitor != null) {
                //(Performance and Visitor are already in DB)
                ticket.setPerformance(performance);
                ticket.setVisitor(visitor);
                Ticket oldTicket = replaceWith(ticket);
                if (oldTicket != null) {
                    //(Ticket with the same key was in DB)

                    oldTicket.getPerformance().getTickets().remove(oldTicket);
                    ticket.getPerformance().getTickets().add(ticket);

                    oldTicket.getVisitor().getTickets().remove(ticket);
                    ticket.getVisitor().getTickets().remove(ticket);

                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Deletes key-value pair
     * @param key - key
     * @return 'true' if pair with this key was successfully deleted, 'false' otherwise
     */
    @Override
    public boolean delete(Integer key) {
        synchronized(InMemoryDB.LOCKER) {
            Ticket ticket = deleteBy(key);
            if (ticket != null) {
                //delete this Ticket from the tickets list of Performance:
                ticket.getPerformance().getTickets().remove(ticket);
                //delete this Ticket from the tickets list of Visitor:
                ticket.getVisitor().getTickets().remove(ticket);
                return true;
            } else {
                return false;
            }
        }
    }

}
