package ua.org.oa.shadow.of.play.cinema.model;

/**
 * Created by volodymyr on 07.02.17.
 *
 * Class represents cinema ticket.
 */
public class Ticket extends Entity {

    private Performance performance;
    private Visitor visitor;
    private int rowNumber;
    private int seatNumber;
    private int price;

    /**
     * Creates new Ticket object
     */
    public Ticket() {
    }

    /**
     * Creates new Ticket object
     * @param performance - performance, this ticket for
     * @param visitor - ticket's owner
     * @param rowNumber - number of the row in the hall
     * @param seatNumber - number of the seat in the row
     * @param price - ticket's price
     */
    public Ticket(Performance performance, Visitor visitor, int rowNumber, int seatNumber, int price) {
        this.performance = performance;
        this.visitor = visitor;
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.price = price;
    }

    public Performance getPerformance() {
        return performance;
    }

    public void setPerformance(Performance performance) {
        this.performance = performance;
    }

    public Visitor getVisitor() {
        return visitor;
    }

    public void setVisitor(Visitor visitor) {
        this.visitor = visitor;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + getId() +
                ", performance=" + performance +
                ", visitor=" + (visitor == null ? "null" : visitor.getId()) +
                ", rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", price=" + price +
                '}';
    }
}
