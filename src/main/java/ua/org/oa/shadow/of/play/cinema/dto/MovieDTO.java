package ua.org.oa.shadow.of.play.cinema.dto;

import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class represents movie (with list of performances).
 */
public class MovieDTO extends SimpleMovieDTO {

    private List<SimplePerformanceDTO> simplePerformancesDTO;

    /**
     * Creates new MovieDTO object
     */
    public MovieDTO() {
    }

    /**
     * Creates new MovieDTO object
     * @param movieTitle - movie title
     * @param movieDescription - movie description
     */
    public MovieDTO(String movieTitle, String movieDescription) {
        super(movieTitle, movieDescription);
    }

    public List<SimplePerformanceDTO> getSimplePerformancesDTO() {
        return simplePerformancesDTO;
    }

    public void setSimplePerformancesDTO(List<SimplePerformanceDTO> simplePerformancesDTO) {
        this.simplePerformancesDTO = simplePerformancesDTO;
    }

    @Override
    public String toString() {
        return "MovieDTO{" +
                "id=" + getId() +
                ", movieTitle='" + getMovieTitle() + '\'' +
                ", originalMovieTitle='" + getOriginalMovieTitle() + '\'' +
                ", director='" + getDirector() + '\'' +
                ", screenwriter='" + getScreenwriter() + '\'' +
                ", actors='" + getActors() + '\'' +
                '}';
    }

}

