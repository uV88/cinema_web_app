package ua.org.oa.shadow.of.play.cinema.dto;

import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class represents visitor profile (with list of tickets).
 */
public class VisitorDTO extends SimpleVisitorDTO {

    private List<TicketDTO> ticketsDTO;

    /**
     * Creates new VisitorDTO object
     */
    public VisitorDTO() {
    }

    /**
     * Creates new VisitorDTO object
     * @param login - visitor's login
     * @param firstName - visitor's first name
     * @param emailAddress - visitor's email address
     * @param password - visitor's password
     * @param role - visitor's role
     */
    public VisitorDTO(String login, String firstName, String emailAddress, String password, Role role) {
        super(login, firstName, emailAddress, password, role);
    }

    public List<TicketDTO> getTicketsDTO() {
        return ticketsDTO;
    }

    public void setTicketsDTO(List<TicketDTO> ticketsDTO) {
        this.ticketsDTO = ticketsDTO;
    }

    @Override
    public String toString() {
        return "VisitorDTO{" +
                "id=" + getId() +
                ", login='" + getLogin() + '\'' +
                ", firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", emailAddress='" + getEmailAddress() + '\'' +
                ", password='" + getPassword() + '\'' +
                '}';
    }

}
