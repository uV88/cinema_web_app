package ua.org.oa.shadow.of.play.cinema.dao.inmemorydb;

import ua.org.oa.shadow.of.play.cinema.dao.api.Dao;
import ua.org.oa.shadow.of.play.cinema.model.Entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Abstract class implements Dao interface, creates data storage in RAM.
 */
public abstract class InMemoryDB<V extends Entity> implements Dao<Integer, V>, Iterable<V> {

    protected static final Object LOCKER = new Object();

    private static final int STORAGE_RANGE = 10;

    private Pair<V>[] storage = (Pair<V>[]) new Pair[InMemoryDB.STORAGE_RANGE];
    private int newPairId = 1;

    protected InMemoryDB() {
    }

    /**
     * Adds new value to this storage
     * @param value - object for adding
     */
    @Override
    public void save(V value) {
        synchronized(LOCKER) {
            if (value != null) {
                Pair<V> newPair = new Pair<>(newPairId, value);
                add(newPair);
                value.setId(newPairId++);
            }
        }
    }

    private void add(Pair<V> newPair) {
        int index = newPair.key % storage.length;
        if(storage[index] != null) {
            newPair.nextPair = storage[index];
        }
        storage[index] = newPair;
    }

    /**
     * Returns value by its key
     * @param key - key, associated with object
     * @return object according to the key
     */
    @Override
    public V getById(Integer key) {
        synchronized(LOCKER) {
            Pair<V> pair = storage[key % storage.length];
            while (pair != null) {
                if (pair.key.equals(key)) {
                    return pair.value;
                }
                pair = pair.nextPair;
            }
            return null;
        }
    }

    /**
     * Returns all values
     * @return all values as list
     */
    @Override
    public List<V> getAll() {
        synchronized(LOCKER) {
            List<V> list = new ArrayList<>();
            for (Pair<V> pair : storage) {
                while (pair != null) {
                    list.add(pair.value);
                    pair = pair.nextPair;
                }
            }
            return list;
        }
    }

    /**
     * Replaces value by key with new object
     * @param value - new object
     * @return 'true' if element with the same key was successfully updated, 'false' otherwise
     */
    @Override
    public boolean update(V value) {
        synchronized(LOCKER) {
            return replaceWith(value) != null;
        }
    }

    protected V replaceWith(V value) {
        Pair<V> pair = storage[value.getId() % storage.length];
        while(pair != null) {
            if (pair.key.equals(value.getId())) {
                V oldValue = pair.value;
                pair.value = value;
                return oldValue;
            }
            pair = pair.nextPair;
        }
        return null;
    }

    /**
     * Deletes key-value pair
     * @param key - key
     * @return 'true' if pair with this key was successfully deleted, 'false' otherwise
     */
    @Override
    public boolean delete(Integer key) {
        synchronized(LOCKER) {
            return deleteBy(key) != null;
        }
    }

    protected V deleteBy(Integer key) {
        int index = key % storage.length;
        if(storage[index] == null) {
            return null;
        }
        else {
            if(storage[index].key.equals(key)) {
                Pair<V> pair = storage[index];
                storage[index] = storage[index].nextPair;
                return pair.value;
            }
            else {
                Pair<V> pair = storage[index];
                while(pair.nextPair != null) {
                    Pair<V> previouslyPair = pair;
                    pair = pair.nextPair;
                    if(pair.key.equals(key)) {
                        previouslyPair.nextPair = pair.nextPair;
                        return pair.value;
                    }
                }
                return null;
            }
        }
    }

    /**
     * Returns an iterator over elements of type V
     * @return an Iterator
     */
    @Override
    public Iterator<V> iterator() {
        return new IteratorImpl();
    }

    private class Pair<T> {

        private final Integer key;
        private T value;
        private Pair<T> nextPair;

        /**
         * Creates new Pair object
         * @param key - key
         * @param value - value
         */
        private Pair(Integer key, T value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * Inner class IteratorImpl
     */
    private class IteratorImpl implements Iterator<V> {

        int nextStorageCellAtCursor = -1;
        Pair<V> nextPairAtCursor = null;

        {
            findNextFullCell();
        }

        /**
         * Returns true if the iteration has more elements.
         * (In other words, returns true if next() would return an element rather than throwing an exception.)
         * @return true if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return nextPairAtCursor!= null;
        }
        /**
         * Returns the next element in the iteration.
         * @return the next element in the iteration
         */
        @Override
        public V next() {
            if(nextPairAtCursor == null) {
                throw new NoSuchElementException();
            }
            Pair<V> pairAtCursor = nextPairAtCursor;
            nextPairAtCursor = nextPairAtCursor.nextPair;
            if(nextPairAtCursor == null) {
                findNextFullCell();
            }
            return pairAtCursor.value;
        }

        private void findNextFullCell() {
            while (++nextStorageCellAtCursor < storage.length) {
                if(storage[nextStorageCellAtCursor] != null) {
                    nextPairAtCursor = storage[nextStorageCellAtCursor];
                    break;
                }
            }
        }

        /**
         * Removes from the underlying collection the last element returned by this iterator.
         * Unsupported operation.
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}
