package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;
import ua.org.oa.shadow.of.play.cinema.dto.VisitorDTO;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by volodymyr on 02.04.17.
 *
 * Servlet prepares single visitor profile or list of profiles for observing.
 */
@WebServlet(name = "VisitorServlet", urlPatterns = "/visitor")
public class VisitorServlet extends HttpServlet {

    /**
     * Calls doGet(request, response).
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Prepares single visitor profile or list of profiles for observing.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String visitorIdStr = request.getParameter("id");
        if(visitorIdStr != null && visitorIdStr.length() != 0) {
            Integer visitorId = Integer.valueOf(visitorIdStr);
            VisitorDTO visitorDTO = ServiceFactory.getVisitorDTOService().getById(visitorId);
            if(visitorDTO != null) {
                request.setAttribute("visitorDTO", visitorDTO);
                request.getRequestDispatcher("pages/common/visitor.jsp").forward(request, response);
            }
            else {
                //visitor with this 'id' does not exist
                response.sendRedirect(request.getContextPath() + "/visitor");
            }
        }
        else {
            List<SimpleVisitorDTO> simpleVisitorDTOList = ServiceFactory.getSimpleVisitorDTOService().getAll();
            request.setAttribute("simpleVisitorDTOList", simpleVisitorDTOList);
            request.getRequestDispatcher("pages/admin/visitors.jsp").forward(request, response);
        }
    }
}
