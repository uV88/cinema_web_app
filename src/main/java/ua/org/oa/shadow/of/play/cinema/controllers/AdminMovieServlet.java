package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.dto.SimpleMovieDTO;
import ua.org.oa.shadow.of.play.cinema.checkers.HtmlReplacer;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 22.03.17.
 *
 * Servlet deletes, updates and adds "movie" object.
 */
@WebServlet(name = "AdminMovieServlet", urlPatterns = {"/delmovie", "/purposemovie", "/implmovie"})
public class AdminMovieServlet extends HttpServlet {

    /**
     * Method makes operation according to servlet path of request.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destination = request.getServletPath();
        switch (destination) {
            case "/delmovie":
                deleteMovie(request, response);
                break;
            case "/purposemovie":
                prepareMovie(request, response);
                break;
            case "/implmovie":
                implementMovie(request, response);
        }
    }

    /**
     * Ignores request, sends redirect to observe list of "movies".
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/movie");
    }

    /**
     * Deletes "movie" by it's key.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void deleteMovie(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer movieId = Integer.valueOf(request.getParameter("movieId"));
        String message;
        String returnUrl;
        if(ServiceFactory.getSimpleMovieDTOService().delete(movieId)) {
            message = "Movie has been successfully deleted!";
            returnUrl = request.getContextPath() + "/movie";
        }
        else {
            message = "Something went wrong!";
            returnUrl = request.getContextPath() + "/movie?id=" + movieId;
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Prepares "movie" for updating or creates template for new "movie".
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void prepareMovie(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SimpleMovieDTO simpleMovieDTO;
        String movieId = request.getParameter("movieId");
        if(movieId != null) {
            Integer id = Integer.valueOf(movieId);
            simpleMovieDTO = ServiceFactory.getSimpleMovieDTOService().getById(id);
            //to prevent NullPointerException:
            if(simpleMovieDTO == null) {
                request.setAttribute("message", "Movie doesn't exists!");
                request.setAttribute("returnUrl", request.getContextPath() + "/movie");
                request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
                return;
            }
        }
        else {
            simpleMovieDTO = new SimpleMovieDTO();
            simpleMovieDTO.setId(-1);
        }
        request.setAttribute("simpleMovieDTO", simpleMovieDTO);
        request.getRequestDispatcher("pages/admin/chMovie.jsp").forward(request, response);
    }

    /**
     * Updates "movie" in DB or adds new one.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the request
     */
    private void implementMovie(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SimpleMovieDTO simpleMovieDTO = new SimpleMovieDTO();
        simpleMovieDTO.setId(Integer.valueOf(request.getParameter("movieId")));
        simpleMovieDTO.setMovieTitle(
                HtmlReplacer.replaceChars(request.getParameter("title")));
        simpleMovieDTO.setOriginalMovieTitle(
                HtmlReplacer.replaceChars(request.getParameter("orTitle")));
        simpleMovieDTO.setPosterUrl(request.getParameter("posterUrl"));
        simpleMovieDTO.setMovieDescription(
                HtmlReplacer.replaceChars(request.getParameter("description")));
        simpleMovieDTO.setDirector(
                HtmlReplacer.replaceChars(request.getParameter("director")));
        simpleMovieDTO.setScreenwriter(
                HtmlReplacer.replaceChars(request.getParameter("screenwriter")));
        simpleMovieDTO.setActors(
                HtmlReplacer.replaceChars(request.getParameter("actors")));

        String message;
        String returnUrl;
        if(simpleMovieDTO.getId() > 0) {
            if(ServiceFactory.getSimpleMovieDTOService().update(simpleMovieDTO)) {
                message = "Movie has been successfully updated!";
            }
            else {
                message = "Something went wrong!";
            }
            returnUrl = request.getContextPath() + "/movie?id=" + simpleMovieDTO.getId();
        }
        else {
            ServiceFactory.getSimpleMovieDTOService().save(simpleMovieDTO);
            if(simpleMovieDTO.getId() > 0) {
                message = "Movie has been successfully added!";
                returnUrl = request.getContextPath() + "/movie?id=" + simpleMovieDTO.getId();
            }
            else {
                message = "Something went wrong!";
                returnUrl = request.getContextPath() + "/movie";
            }
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }
}
