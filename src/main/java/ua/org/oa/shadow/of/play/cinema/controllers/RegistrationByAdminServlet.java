package ua.org.oa.shadow.of.play.cinema.controllers;

import ua.org.oa.shadow.of.play.cinema.checkers.EmailChecker;
import ua.org.oa.shadow.of.play.cinema.dto.SimpleVisitorDTO;
import ua.org.oa.shadow.of.play.cinema.checkers.HtmlReplacer;
import ua.org.oa.shadow.of.play.cinema.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by volodymyr on 02.04.17.
 *
 * Servlet adds new visitor's profile (USER or ADMIN).
 */
@WebServlet(name = "RegistrationByAdminServlet", urlPatterns = "/adminregistration")
public class RegistrationByAdminServlet extends HttpServlet {

    /**
     * Adds new visitor (USER or ADMIN) profile to DB.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the POST could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the POST request
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message;
        String returnUrl;
        String password = request.getParameter("password");
        if(request.getParameter("confPassword").equals(password)) {
            String emailAddress = request.getParameter("emailAddress");
            if(EmailChecker.isEmailAddress(emailAddress)) {
                SimpleVisitorDTO simpleVisitorDTO = new SimpleVisitorDTO();
                simpleVisitorDTO.setLogin(HtmlReplacer.replaceChars(request.getParameter("login")));
                simpleVisitorDTO.setPassword(password);
                simpleVisitorDTO.setEmailAddress(HtmlReplacer.replaceChars(emailAddress));
                simpleVisitorDTO.setFirstName(HtmlReplacer.replaceChars(request.getParameter("firstName")));
                simpleVisitorDTO.setLastName(HtmlReplacer.replaceChars(request.getParameter("lastName")));
                simpleVisitorDTO.setRole(SimpleVisitorDTO.Role.valueOf(request.getParameter("role")));
                simpleVisitorDTO.setBlocked(false);

                ServiceFactory.getSimpleVisitorDTOService().save(simpleVisitorDTO);
                if(simpleVisitorDTO.getId() > 0) {
                    message = "Visitor has been successfully added!";
                    returnUrl = request.getContextPath() + "/visitor?id=" + simpleVisitorDTO.getId();
                }
                else {
                    message = "Something went wrong!";
                    returnUrl = request.getContextPath() + "/visitor";
                }
            }
            else {
                message = "Incorrect mail address!";
                returnUrl = request.getContextPath() + "/adminregistration";
            }
        }
        else {
            message = "Values of \"Password\" and \"Confirm password\" fields are not the same!";
            returnUrl = request.getContextPath() + "/adminregistration";
        }
        request.setAttribute("message", message);
        request.setAttribute("returnUrl", returnUrl);
        request.getRequestDispatcher("pages/common/message.jsp").forward(request, response);
    }

    /**
     * Creates template for new visitor's profile.
     * @param request - an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response - an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws ServletException - if the request for the GET could not be handled
     * @throws IOException - if an input or output error is detected when the servlet handles the GET request
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("pages/admin/regvisitor.jsp").forward(request, response);
    }
}
