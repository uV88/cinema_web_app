package ua.org.oa.shadow.of.play.cinema.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by volodymyr on 08.03.17.
 *
 * Class represents cinema performance (with list of tickets).
 */
public class PerformanceDTO extends SimplePerformanceDTO {

    private List<TicketDTO> ticketsDTO;

    /**
     * Creates new PerformanceDTO object
     */
    public PerformanceDTO() {
    }

    /**
     * Creates new PerformanceDTO object
     * @param date - date of the performance
     * @param time - time of the performance
     * @param simpleHallDTO - performance's simpleHallDTO
     * @param simpleMovieDTO - performance's simpleMovieDTO
     */
    public PerformanceDTO(LocalDate date, LocalTime time, SimpleHallDTO simpleHallDTO, SimpleMovieDTO simpleMovieDTO) {
        super(date, time, simpleHallDTO, simpleMovieDTO);
    }

    public List<TicketDTO> getTicketsDTO() {
        return ticketsDTO;
    }

    public void setTicketsDTO(List<TicketDTO> ticketsDTO) {
        this.ticketsDTO = ticketsDTO;
    }

    @Override
    public String toString() {
        return "PerformanceDTO{" +
                "id=" + getId() +
                ", date=" + getDate() +
                ", time=" + getTime() +
                ", hallDTO=" + (getSimpleHallDTO() == null ? "null" : getSimpleHallDTO().getHallName()) +
                ", movieDTO=" + (getSimpleMovieDTO() == null ? "null" : getSimpleMovieDTO().getMovieTitle()) +
                '}';
    }
}
